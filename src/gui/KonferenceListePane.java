package gui;

import java.util.ArrayList;

import application.model.Deltager;
import application.model.Konference;
import application.model.Tilstand;
import application.service.Service;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;

public class KonferenceListePane extends Pane {
	private ListView<Konference> lvwKonferencer;
	private TextField[] txfInfo;

	public KonferenceListePane() {
		this.lvwKonferencer = new ListView<>();
		this.lvwKonferencer.setPrefSize(300, 500);
		this.add(this.lvwKonferencer, 0, 0);

		String[] labelNavne = { "Titel", "Land", "By", "Adresse", "Start dato", "Slut dato", "Pris", "Kontakt" };
		Label[] lblInfo = new Label[labelNavne.length];
		this.txfInfo = new TextField[labelNavne.length];

		GridPane infoGrid = new GridPane();
		infoGrid.setPadding(new Insets(20));
		infoGrid.setHgap(20);
		infoGrid.setVgap(10);

		this.add(infoGrid, 1, 0);
		for (int i = 0; i < lblInfo.length; i++) {
			lblInfo[i] = new Label();
			lblInfo[i].setText(labelNavne[i]);
			infoGrid.add(lblInfo[i], 0, i);

			this.txfInfo[i] = new TextField();
			this.txfInfo[i].setDisable(true);
			infoGrid.add(txfInfo[i], 1, i);

		}

		Button btnTilmeld = new Button();
		btnTilmeld.setText("Tilmeld Konference");
		btnTilmeld.setOnAction(event -> this.tilmeldAction());
		infoGrid.add(btnTilmeld, 1, labelNavne.length);
		this.lvwKonferencer.setOnMouseClicked(e -> this.selectionChanged());

	}

	public void updateControls() {
		this.lvwKonferencer.getItems().clear();
		ArrayList<Konference> konferencer = new ArrayList<>();
		konferencer.clear();
		ArrayList<Konference> dataKonference = Service.getKonferencer();

		for (int i = 0; i < dataKonference.size(); i++) {
			if (dataKonference.get(i).getTilstand() == Tilstand.ÅBEN) {
				if (!inKonference(dataKonference.get(i))) {
					konferencer.add(dataKonference.get(i));
				}
			}
		}

		this.lvwKonferencer.getItems().addAll(konferencer);
	}

	// Fix datetime format.

	public void selectionChanged() {
		Konference konference = this.lvwKonferencer.getSelectionModel().getSelectedItem();
		if (konference != null) {
			this.txfInfo[0].setText(konference.getTitel());
			this.txfInfo[1].setText(konference.getLand());
			this.txfInfo[2].setText(konference.getPostNr() + " " + konference.getBy());
			this.txfInfo[3]
					.setText(konference.getVejNavn() + " " + konference.getVejNr() + " " + konference.getEtage());
			this.txfInfo[4].setText(konference.getStartDato().getDayOfMonth() + "/"
					+ konference.getStartDato().getMonthValue() + "/" + konference.getStartDato().getYear() + " - "
					+ konference.getStartDato().getHour() + ":" + konference.getStartDato().getMinute());
			this.txfInfo[5].setText(konference.getSlutDato().getDayOfMonth() + "/"
					+ konference.getSlutDato().getMonthValue() + "/" + konference.getSlutDato().getYear() + " - "
					+ konference.getSlutDato().getHour() + ":" + konference.getSlutDato().getMinute());
			this.txfInfo[6].setText("" + konference.getPris());
			this.txfInfo[7].setText("" + konference.getTlfNr());
		}
	}

	public void tilmeldAction() {
		Konference konference = this.lvwKonferencer.getSelectionModel().getSelectedItem();
		if (konference != null) {
			TilmeldKonferenceWindow tkwindow = new TilmeldKonferenceWindow("Tilmeld Konference", konference, null);
			tkwindow.initModality(Modality.APPLICATION_MODAL);
			tkwindow.showAndWait();
			this.updateControls();
		}
	}

	public boolean inKonference(Konference konference) {
		Deltager deltager = Service.deltagerLoggedIn;
		for (int i = 0; i < konference.getTilmeldeinger().size(); i++) {
			if (konference.getTilmeldeinger().get(i).getDeltager().equals(deltager)) {
				return true;
			}
		}
		return false;
	}
}
