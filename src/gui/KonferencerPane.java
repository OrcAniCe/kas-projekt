package gui;

import java.util.ArrayList;
import java.util.Optional;

import application.model.Foredrag;
import application.model.Hotel;
import application.model.Konference;
import application.model.Udflugt;
import application.service.Service;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;

public class KonferencerPane extends Pane {
	private ListView<Konference> lvwKonferencer;
	private ListView<Foredrag> lvwForedrag;
	private ListView<Hotel> lvwHoteller;
	private ListView<Udflugt> lvwUdflugter;
	private Label lblError;

	public KonferencerPane() {

		// Konferencer
		Label lblKonferencer = new Label();
		lblKonferencer.setText("Konferencer:");
		this.add(lblKonferencer, 0, 0);

		this.lvwKonferencer = new ListView<>();
		this.lvwKonferencer.setPrefSize(250, 350);
		this.lvwKonferencer.setMinWidth(350);
		this.add(this.lvwKonferencer, 0, 1);

		HBox hBoxKonferenceBtn = new HBox();
		hBoxKonferenceBtn.setSpacing(10);

		Button btnCreateKonference = new Button();
		btnCreateKonference.setText("Opret");
		btnCreateKonference.setOnAction(e -> this.createKonference());

		Button btnUpdateKonference = new Button();
		btnUpdateKonference.setText("�ndre");
		btnUpdateKonference.setOnAction(e -> this.updateKonference());

		Button btnDeleteKonference = new Button();
		btnDeleteKonference.setText("Slet");
		btnDeleteKonference.setOnAction(e -> this.deleteKonference());

		Button btnListe = new Button();
		btnListe.setText("Liste");
		btnListe.setOnAction(e -> this.listeKonference());

		hBoxKonferenceBtn.getChildren().addAll(btnCreateKonference, btnUpdateKonference, btnDeleteKonference, btnListe);
		this.add(hBoxKonferenceBtn, 0, 2);
		this.lvwKonferencer.setOnMouseClicked(e -> this.konferenceSelectionChanged());

		// Foredrag
		Label lblForedrag = new Label();
		lblForedrag.setText("Foredrag:");
		this.add(lblForedrag, 0, 3);

		this.lvwForedrag = new ListView<>();
		this.lvwForedrag.setPrefSize(250, 350);
		this.add(this.lvwForedrag, 0, 4);

		HBox hBoxForedragBtn = new HBox();
		hBoxForedragBtn.setSpacing(10);

		Button btnCreateForedrag = new Button();
		btnCreateForedrag.setText("Opret");
		btnCreateForedrag.setOnAction(e -> this.createForedrag());

		Button btnUpdateForedrag = new Button();
		btnUpdateForedrag.setText("�ndre");
		btnUpdateForedrag.setOnAction(e -> this.updateForedrag());

		Button btnDeleteForedrag = new Button();
		btnDeleteForedrag.setText("Slet");
		btnDeleteForedrag.setOnAction(e -> this.deleteKonference());
		hBoxForedragBtn.getChildren().addAll(btnCreateForedrag, btnUpdateForedrag, btnDeleteForedrag);
		this.add(hBoxForedragBtn, 0, 5);

		// Hoteller
		Label lblHoteller = new Label();
		lblHoteller.setText("Hoteller:");
		this.add(lblHoteller, 1, 0);

		this.lvwHoteller = new ListView<>();
		this.lvwHoteller.setPrefSize(250, 350);
		this.lvwHoteller.setMinWidth(350);
		this.add(this.lvwHoteller, 1, 1);

		HBox hBoxHotellerBtn = new HBox();
		hBoxHotellerBtn.setSpacing(10);

		Button btnCreateHotel = new Button();
		btnCreateHotel.setText("Opret");
		btnCreateHotel.setOnAction(e -> this.createHotel());

		Button btnUpdateHotel = new Button();
		btnUpdateHotel.setText("�ndre");
		btnUpdateHotel.setOnAction(e -> this.updateHotel());

		Button btnDeleteHotel = new Button();
		btnDeleteHotel.setText("Slet");
		btnDeleteHotel.setOnAction(e -> this.deleteHotel());

		Button btnListeHoteller = new Button();
		btnListeHoteller.setText("Liste");
		btnListeHoteller.setOnAction(e -> this.listeHoteller());

		hBoxHotellerBtn.getChildren().addAll(btnCreateHotel, btnUpdateHotel, btnDeleteHotel, btnListeHoteller);
		this.add(hBoxHotellerBtn, 1, 2);

		// Udflugter
		Label lblUdflugter = new Label();
		lblUdflugter.setText("Udflugter:");
		this.add(lblUdflugter, 1, 3);

		this.lvwUdflugter = new ListView<>();
		this.lvwUdflugter.setPrefSize(250, 350);
		this.lvwUdflugter.setMinWidth(350);
		this.add(this.lvwUdflugter, 1, 4);

		HBox hBoxUdflugterBtn = new HBox();
		hBoxUdflugterBtn.setSpacing(10);

		Button btnCreateUdflugt = new Button();
		btnCreateUdflugt.setText("Opret");
		btnCreateUdflugt.setOnAction(e -> this.createUdflugt());

		Button btnUpdateUdflugt = new Button();
		btnUpdateUdflugt.setText("�ndre");
		btnUpdateUdflugt.setOnAction(e -> this.updateUdflugt());

		Button btnDeleteUdflugt = new Button();
		btnDeleteUdflugt.setText("Slet");
		btnDeleteUdflugt.setOnAction(e -> this.deleteUdflugt());

		Button btnListeUdflugter = new Button();
		btnListeUdflugter.setText("Liste");
		btnListeUdflugter.setOnAction(e -> this.listeUdflugter());

		hBoxUdflugterBtn.getChildren().addAll(btnCreateUdflugt, btnUpdateUdflugt, btnDeleteUdflugt, btnListeUdflugter);
		this.add(hBoxUdflugterBtn, 1, 5);

		this.lblError = new Label();
		this.add(this.lblError, 1, 6);
		this.lblError.setStyle("-fx-text-fill: red");

	}

	public void updateControls() {
		this.lblError.setText("");
		this.lvwKonferencer.getItems().clear();
		ArrayList<Konference> konferencer = new ArrayList<>();

		for (int i = Service.getKonferencer().size() - 1; i > -1; i--) {
			konferencer.add(Service.getKonferencer().get(i));
		}

		this.lvwKonferencer.getItems().addAll(konferencer);
	}

	public void konferenceSelectionChanged() {
		Konference konference = this.lvwKonferencer.getSelectionModel().getSelectedItem();

		if (konference != null) {
			this.lvwHoteller.getItems().clear();
			this.lvwUdflugter.getItems().clear();
			this.lvwForedrag.getItems().clear();
			this.lvwHoteller.getItems().addAll(konference.getHoteller());
			this.lvwUdflugter.getItems().addAll(konference.getUdflugter());
			this.lvwForedrag.getItems().addAll(konference.getForedrag());
		} else {
			return;
		}

	}

	// Funktioner konferencer
	public void createKonference() {
		KonferenceCreateUpdateWindow konferenceCreateUpdateWindow = new KonferenceCreateUpdateWindow("Opret konference",
				null);
		konferenceCreateUpdateWindow.initModality(Modality.APPLICATION_MODAL);
		konferenceCreateUpdateWindow.showAndWait();
		this.updateControls();
	}

	public void updateKonference() {
		Konference konference = this.lvwKonferencer.getSelectionModel().getSelectedItem();
		if (konference != null) {
			KonferenceCreateUpdateWindow konferenceCreateUpdateWindow = new KonferenceCreateUpdateWindow(
					"�ndre konference", konference);
			konferenceCreateUpdateWindow.initModality(Modality.APPLICATION_MODAL);
			konferenceCreateUpdateWindow.showAndWait();
			this.updateControls();
		} else {
			this.lblError.setText("Du skal v�lge en konference!");
		}
	}

	public void deleteKonference() {
		Konference konference = this.lvwKonferencer.getSelectionModel().getSelectedItem();

		if (konference != null) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Slet konference");
			alert.setHeaderText("Slet konference");
			alert.setContentText("Er du sikker p� du vil slette konferencen?");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
				Service.removeKonference(konference);
				this.updateControls();
			} else {
				return;
			}
		}
	}

	public void listeKonference() {
		Konference konference = this.lvwKonferencer.getSelectionModel().getSelectedItem();
		if (konference != null) {
			ListeKonferenceWindow listeKonferenceWindow = new ListeKonferenceWindow("Liste", konference);
			listeKonferenceWindow.initModality(Modality.APPLICATION_MODAL);
			listeKonferenceWindow.showAndWait();
		} else {
			this.lblError.setText("Du skal v�lge en konference!");
		}
	}

	// Funktioner foredrag
	public void createForedrag() {
		Konference konference = this.lvwKonferencer.getSelectionModel().getSelectedItem();
		if (konference != null) {
			ForedragCreateUpdateWindow foredragCreateUpdateWindow = new ForedragCreateUpdateWindow("Opret foredrag",
					null, konference);
			foredragCreateUpdateWindow.initModality(Modality.APPLICATION_MODAL);
			foredragCreateUpdateWindow.showAndWait();
			this.konferenceSelectionChanged();
		} else {
			this.lblError.setText("Du skal v�lge en konference f�r du kan oprette et foredrag");
		}
	}

	public void updateForedrag() {
		Foredrag foredrag = this.lvwForedrag.getSelectionModel().getSelectedItem();
		Konference konference = this.lvwKonferencer.getSelectionModel().getSelectedItem();
		if (foredrag != null) {
			ForedragCreateUpdateWindow foredragCreateUpdateWindow = new ForedragCreateUpdateWindow("�ndre foredrag",
					foredrag, konference);
			foredragCreateUpdateWindow.initModality(Modality.APPLICATION_MODAL);
			foredragCreateUpdateWindow.showAndWait();
			this.konferenceSelectionChanged();
		} else {
			this.lblError.setText("Du skal v�lge et foredrag du vil �ndre");
		}
	}

	public void deleteForedrag() {
		Konference konference = this.lvwKonferencer.getSelectionModel().getSelectedItem();
		Foredrag foredrag = this.lvwForedrag.getSelectionModel().getSelectedItem();

		if (foredrag != null && konference != null) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Slet foredrag");
			alert.setHeaderText("Slet foredrag");
			alert.setContentText("Er du sikker p� du vil slette foredraget?");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
				Service.removeForedrag(konference, foredrag);
				this.updateControls();
			} else {
				return;
			}
		}
	}

	public void createHotel() {
		Konference konference = this.lvwKonferencer.getSelectionModel().getSelectedItem();
		if (konference != null) {
			HotelCreateUpdateWindow hotelCreateUpdateWindow = new HotelCreateUpdateWindow("Opret hotel", null,
					konference);
			hotelCreateUpdateWindow.initModality(Modality.APPLICATION_MODAL);
			hotelCreateUpdateWindow.showAndWait();
			this.konferenceSelectionChanged();
		} else {
			this.lblError.setText("Du skal v�lge en konference f�r du kan oprette et hotel");
		}
	}

	// Funktioner hoteller
	public void updateHotel() {
		Hotel hotel = this.lvwHoteller.getSelectionModel().getSelectedItem();
		Konference konference = this.lvwKonferencer.getSelectionModel().getSelectedItem();
		if (hotel != null) {
			HotelCreateUpdateWindow hotelCreateUpdateWindow = new HotelCreateUpdateWindow("�ndre hotel", hotel,
					konference);
			hotelCreateUpdateWindow.initModality(Modality.APPLICATION_MODAL);
			hotelCreateUpdateWindow.showAndWait();
			this.konferenceSelectionChanged();
		} else {
			this.lblError.setText("Du skal v�lge et hotel du vil �ndre");
		}
	}

	public void deleteHotel() {
		Konference konference = this.lvwKonferencer.getSelectionModel().getSelectedItem();
		Hotel hotel = this.lvwHoteller.getSelectionModel().getSelectedItem();

		if (hotel != null && konference != null) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Slet hotel");
			alert.setHeaderText("Slet hotel");
			alert.setContentText("Er du sikker p� du vil slette hotellet?");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
				Service.removeHotel(konference, hotel);
				this.konferenceSelectionChanged();
			} else {
				return;
			}
		}
	}

	public void listeHoteller() {
		Konference konference = this.lvwKonferencer.getSelectionModel().getSelectedItem();
		if (konference != null) {
			ListeHotellerWindow listeHotellerWindow = new ListeHotellerWindow("Liste", konference);
			listeHotellerWindow.initModality(Modality.APPLICATION_MODAL);
			listeHotellerWindow.showAndWait();
		} else {
			this.lblError.setText("Du skal v�lge en konference!");
		}
	}

	// Funktioner udflugter
	public void createUdflugt() {
		Konference konference = this.lvwKonferencer.getSelectionModel().getSelectedItem();
		if (konference != null) {
			UdflugtCreateUpdateWindow udflugtCreateUpdateWindow = new UdflugtCreateUpdateWindow("Opret udflugt", null,
					konference);
			udflugtCreateUpdateWindow.initModality(Modality.APPLICATION_MODAL);
			udflugtCreateUpdateWindow.showAndWait();
			this.konferenceSelectionChanged();
		} else {
			this.lblError.setText("Du skal v�lge en konference f�r du kan oprette en udflugt");
		}
	}

	public void updateUdflugt() {
		Udflugt udflugt = this.lvwUdflugter.getSelectionModel().getSelectedItem();
		Konference konference = this.lvwKonferencer.getSelectionModel().getSelectedItem();
		if (udflugt != null) {
			UdflugtCreateUpdateWindow udflugtCreateUpdateWindow = new UdflugtCreateUpdateWindow("�ndre udflugt",
					udflugt, konference);
			udflugtCreateUpdateWindow.initModality(Modality.APPLICATION_MODAL);
			udflugtCreateUpdateWindow.showAndWait();
			this.konferenceSelectionChanged();
		} else {
			this.lblError.setText("Du skal v�lge en udflugt du vil �ndre");
		}
	}

	public void deleteUdflugt() {
		Konference konference = this.lvwKonferencer.getSelectionModel().getSelectedItem();
		Udflugt udflugt = this.lvwUdflugter.getSelectionModel().getSelectedItem();

		if (udflugt != null && konference != null) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Slet Udflugt");
			alert.setHeaderText("Slet Udflugt");
			alert.setContentText("Er du sikker p� du vil slette udflugten?");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
				Service.removeUdflugt(konference, udflugt);
				this.konferenceSelectionChanged();
			} else {
				return;
			}
		}
	}

	public void listeUdflugter() {
		Konference konference = this.lvwKonferencer.getSelectionModel().getSelectedItem();
		if (konference != null) {
			ListeUdflugterWindow listeUdflugterWindow = new ListeUdflugterWindow("Liste", konference);
			listeUdflugterWindow.initModality(Modality.APPLICATION_MODAL);
			listeUdflugterWindow.showAndWait();
		} else {
			this.lblError.setText("Du skal v�lge en konference!");
		}
	}

}
