package gui;

import application.model.Deltager;
import application.service.Service;
import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class LoginWindow extends Stage {
	private Stage primaryStage;
	private MainApp mainApp;
	public static int loginTimes = 0;

	public LoginWindow(String title, Stage primaryStage, MainApp mainApp) {
		this.primaryStage = primaryStage;
		this.mainApp = mainApp;

		this.setResizable(false);
		this.setOnCloseRequest(e -> this.onCloseLoginWindow());

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		scene.getStylesheets().add(MainApp.STYLE);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private TextField txfEmail;
	private PasswordField txfPassword;
	private Label lblError;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		Label lblEmail = new Label("Email");
		pane.add(lblEmail, 0, 0);

		txfEmail = new TextField();
		pane.add(txfEmail, 0, 1);
		txfEmail.setPrefWidth(200);

		Label lblPassword = new Label("Password");
		pane.add(lblPassword, 0, 2);

		txfPassword = new PasswordField();
		pane.add(txfPassword, 0, 3);

		Button btnLogin = new Button("Login");
		pane.add(btnLogin, 0, 4);
		GridPane.setHalignment(btnLogin, HPos.LEFT);
		btnLogin.setOnAction(event -> this.loginAction());

		Button btnCreateUser = new Button("Opret Deltager");
		pane.add(btnCreateUser, 0, 4);
		GridPane.setHalignment(btnCreateUser, HPos.RIGHT);
		btnCreateUser.setOnAction(event -> this.createDeltager());

		lblError = new Label();
		pane.add(lblError, 0, 5);
		lblError.setStyle("-fx-text-fill: red");

		this.initControls();
	}

	private void initControls() {
		this.txfEmail.clear();
		this.txfPassword.clear();
		txfEmail.setText("zynzz@zynzz.dk");
		txfPassword.setText("1234");
	}

	// -------------------------------------------------------------------------

	private void loginAction() {
		Deltager login = Service.login(txfEmail.getText(), txfPassword.getText());
		if (login != null) {
			this.initControls();
			this.hide();
			if (LoginWindow.loginTimes != 0) {
				mainApp.updateLoginInformation();
			}

			LoginWindow.loginTimes++;

			this.primaryStage.show();
		} else {
			this.lblError.setText("Kunne ikke logge ind");
		}
	}

	private void createDeltager() {
		DeltagerCreateUpdateWindow createDeltager = new DeltagerCreateUpdateWindow("Opret", null);
		createDeltager.initModality(Modality.APPLICATION_MODAL);
		createDeltager.showAndWait();
	}

	private void onCloseLoginWindow() {
		Platform.exit();
	}

}
