package gui;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import application.model.Deltager;
import application.model.Foredrag;
import application.model.Konference;
import application.service.Service;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class ForedragCreateUpdateWindow extends Stage {
	private Foredrag foredrag;
	private Konference konference;
	private ListView<Deltager> lvwForedragsHolder;
	private ListView<Deltager> lvwDeltagere;

	public ForedragCreateUpdateWindow(String title, Foredrag foredrag, Konference konference) {

		this.foredrag = foredrag;
		this.konference = konference;

		this.setResizable(false);
		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		scene.getStylesheets().add(MainApp.STYLE);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private TextField txfTitel, txfBeskrivelse;
	private DatePicker dPickStartTid, dPickSlutTid;
	private Label lblError;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		// Textfields
		txfTitel = new TextField();
		txfBeskrivelse = new TextField();

		this.dPickStartTid = new DatePicker();
		this.dPickSlutTid = new DatePicker();

		this.lvwDeltagere = new ListView<>();
		this.lvwDeltagere.setMaxHeight(300);
		this.lvwDeltagere.setOnMouseClicked(e -> opretForedragsholder());
		this.lvwForedragsHolder = new ListView<>();
		this.lvwForedragsHolder.setMaxHeight(300);
		this.lvwForedragsHolder.setOnMouseClicked(e -> sletForedragsholder());

		Label lblTitel = new Label();
		lblTitel.setText("Titel:");

		Label lblBeskrivelse = new Label();
		lblBeskrivelse.setText("Beskrivelse:");

		Label lblStart = new Label();
		lblStart.setText("Start:");

		Label lblSlut = new Label();
		lblSlut.setText("Slut:");

		Label lblForedragsHolder = new Label();
		lblForedragsHolder.setText("Foredragsholder:");

		pane.add(lblTitel, 0, 0);
		pane.add(lblBeskrivelse, 0, 1);
		pane.add(lblStart, 0, 2);
		pane.add(lblSlut, 0, 3);
		pane.add(lblForedragsHolder, 0, 4);

		pane.add(txfTitel, 1, 0);
		pane.add(txfBeskrivelse, 1, 1);
		pane.add(dPickStartTid, 1, 2);
		pane.add(dPickSlutTid, 1, 3);
		pane.add(lvwForedragsHolder, 1, 4);
		pane.add(lvwDeltagere, 2, 4);

		Button btnLogin = new Button("OK");
		pane.add(btnLogin, 1, 5);
		GridPane.setHalignment(btnLogin, HPos.LEFT);
		btnLogin.setOnAction(event -> this.okAction());

		Button btnCreateUser = new Button("Cancel");
		pane.add(btnCreateUser, 1, 5);
		GridPane.setHalignment(btnCreateUser, HPos.RIGHT);
		btnCreateUser.setOnAction(event -> this.close());

		lblError = new Label();
		pane.add(lblError, 1, 6);
		lblError.setStyle("-fx-text-fill: red");

		this.initControls();
	}

	private void initControls() {
		this.lblError.setText("");
		if (this.foredrag != null) {
			this.txfTitel.setText(foredrag.getTitel());
			this.txfBeskrivelse.setText(foredrag.getBeskrivelse());
			this.dPickStartTid.setValue(LocalDate.of(this.foredrag.getStartTid().getYear(),
					this.foredrag.getStartTid().getMonthValue(), this.foredrag.getStartTid().getDayOfMonth()));

			this.dPickSlutTid.setValue(LocalDate.of(this.foredrag.getSlutTid().getYear(),
					this.foredrag.getSlutTid().getMonthValue(), this.foredrag.getSlutTid().getDayOfMonth()));
			this.lvwForedragsHolder.getItems().setAll(this.foredrag.getForedragsholder());
		} else {
			txfTitel.clear();
			txfBeskrivelse.clear();
		}
		ArrayList<Deltager> dataDeltager = Service.getDeltagere();
		ArrayList<Deltager> deltagere = new ArrayList<>();
		if (this.lvwForedragsHolder.getItems().size() != 0) {
			for (int i = 0; i < this.lvwForedragsHolder.getItems().size(); i++) {
				for (int j = 0; j < dataDeltager.size(); j++) {
					if (!this.lvwForedragsHolder.getItems().contains(dataDeltager.get(j))
							&& !deltagere.contains(dataDeltager.get(j))) {
						deltagere.add(dataDeltager.get(j));
					}
				}
			}
		} else {
			deltagere = dataDeltager;
		}

		this.lvwDeltagere.getItems().addAll(deltagere);
	}

	private void okAction() {
		if (!this.txfTitel.getText().isEmpty() && !this.txfBeskrivelse.getText().isEmpty()) {
			if (this.dPickStartTid.getValue() != null && this.dPickSlutTid.getValue() != null) {
				LocalDate sl = this.dPickStartTid.getValue();
				LocalDate st = this.dPickSlutTid.getValue();
				LocalDateTime start = LocalDateTime.of(sl.getYear(), sl.getMonthValue(), sl.getDayOfMonth(), 12, 00);
				LocalDateTime slut = LocalDateTime.of(st.getYear(), st.getMonthValue(), st.getDayOfMonth(), 12, 00);

				if (this.lvwForedragsHolder.getItems().size() != 0) {
					ArrayList<Deltager> foredragsholder = new ArrayList<>();

					for (int i = 0; i < this.lvwForedragsHolder.getItems().size(); i++) {
						foredragsholder.add(this.lvwForedragsHolder.getItems().get(i));
					}

					if (this.foredrag != null) {
						Service.updateForedrag(foredrag, this.txfTitel.getText(), this.txfBeskrivelse.getText(), start,
								slut, foredragsholder);
						this.close();
					} else {
						Service.createForedrag(this.konference, this.txfTitel.getText(), this.txfBeskrivelse.getText(),
								start, slut, foredragsholder);
						this.close();
					}
				} else {
					this.lblError.setText("Du skal v�lge en foredragsholder!");
				}
			} else {
				this.lblError.setText("Indtast ankomst og afrejse");
			}
		} else {
			this.lblError.setText("Indtast titel eller beskrivelse");
		}
	}

	public void opretForedragsholder() {
		Deltager deltager = this.lvwDeltagere.getSelectionModel().getSelectedItem();
		if (deltager != null) {
			this.lvwForedragsHolder.getItems().add(deltager);
			this.lvwDeltagere.getItems().remove(deltager);
		}
	}

	public void sletForedragsholder() {
		Deltager foredragsholder = this.lvwForedragsHolder.getSelectionModel().getSelectedItem();
		if (foredragsholder != null) {
			this.lvwForedragsHolder.getItems().remove(foredragsholder);
			this.lvwDeltagere.getItems().add(foredragsholder);
		}
	}
}
