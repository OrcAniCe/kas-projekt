package gui;

import java.util.ArrayList;
import java.util.Optional;

import application.model.Konference;
import application.model.Tilmelding;
import application.service.Service;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;

public class TilmeldteKonferencerPane extends Pane {
	private ListView<Tilmelding> lvwTilmeldinger;
	private TextArea txtRegning;

	public TilmeldteKonferencerPane() {

		Label lblTilmeldinger = new Label();
		lblTilmeldinger.setText("Dine tilmeldinger:");
		this.add(lblTilmeldinger, 0, 0);

		this.lvwTilmeldinger = new ListView<>();
		this.lvwTilmeldinger.setOnMouseClicked(e -> this.selectedChanged());
		this.add(this.lvwTilmeldinger, 0, 1);

		this.txtRegning = new TextArea();
		this.add(txtRegning, 1, 1);

		Button btnUpdate = new Button();
		btnUpdate.setText("�ndre");
		btnUpdate.setOnAction(e -> this.updateTilmelding());

		Button btnDelete = new Button();
		btnDelete.setText("Slet");
		btnDelete.setOnAction(e -> this.deleteTilmelding());

		HBox hBox = new HBox(10);
		hBox.getChildren().addAll(btnUpdate, btnDelete);

		this.add(hBox, 0, 2);
	}

	public void updateControls() {
		this.lvwTilmeldinger.getItems().clear();
		this.txtRegning.clear();
		ArrayList<Konference> konferencer = new ArrayList<>();
		ArrayList<Tilmelding> mineTilmeldinger = new ArrayList<>();
		konferencer = Service.getKonferencer();

		for (int i = 0; i < konferencer.size(); i++) {
			for (int j = 0; j < konferencer.get(i).getTilmeldeinger().size(); j++) {
				if (konferencer.get(i).getTilmeldeinger().get(j).getDeltager().equals(Service.deltagerLoggedIn)) {
					mineTilmeldinger.add(konferencer.get(i).getTilmeldeinger().get(j));
				}
			}
		}
		this.lvwTilmeldinger.getItems().addAll(mineTilmeldinger);

	}

	public void selectedChanged() {
		Tilmelding tilmelding = this.lvwTilmeldinger.getSelectionModel().getSelectedItem();
		if (tilmelding != null) {
			StringBuilder sb = new StringBuilder();

			// Konference
			sb.append("Konference: (antal dage: " + tilmelding.getDage() + ")\n");
			sb.append(tilmelding.getKonference().getTitel() + " (" + tilmelding.getPrisKonference() + " DKK)\n\n");

			// Hotel
			if (tilmelding.getHotel() != null) {
				sb.append("\nHotel:\n");
				sb.append(tilmelding.getHotel().getNavn() + " (" + tilmelding.getPrisHotel() + " DKK)\n\n");

				// Hotel services
				if (tilmelding.getHotelServices().size() != 0) {
					sb.append("\nHotel services:\n");
					for (int i = 0; i < tilmelding.getHotelServices().size(); i++) {
						sb.append(tilmelding.getHotelServices().get(i).getNavn() + " ("
								+ tilmelding.getHotelServices().get(i).getPris() + " DKK)\n");
					}
					sb.append("Total: " + tilmelding.getPrisHotelServices() + " DKK\n");
				}
			}

			// Udflugter
			if (tilmelding.getUdflugter().size() != 0) {
				sb.append("\nUdflugter:\n");

				for (int i = 0; i < tilmelding.getUdflugter().size(); i++) {
					sb.append(tilmelding.getUdflugter().get(i).getNavn() + " ("
							+ tilmelding.getUdflugter().get(i).getPris() + " DKK)\n");
				}
				sb.append("Total: " + tilmelding.getPrisUdflugter() + " DKK\n");
			}

			sb.append("\nIalt: " + tilmelding.getPrisTotal() + " DKK");
			this.txtRegning.setText(sb.toString());

		}
	}

	public void updateTilmelding() {
		Tilmelding tilmelding = this.lvwTilmeldinger.getSelectionModel().getSelectedItem();
		if (tilmelding != null) {
			TilmeldKonferenceWindow tilmeldKonferenceWindow = new TilmeldKonferenceWindow("�ndre tilmelding",
					tilmelding.getKonference(), tilmelding);
			tilmeldKonferenceWindow.showAndWait();
			this.selectedChanged();
		} else {
			return;
		}
	}

	public void deleteTilmelding() {
		Tilmelding tilmelding = this.lvwTilmeldinger.getSelectionModel().getSelectedItem();

		if (tilmelding != null) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Slet tilmelding");
			alert.setHeaderText("Slet tilmelding");
			alert.setContentText("Er du sikker p� du vil slette din tilmelding?");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
				Service.deleteTilmelding(tilmelding, tilmelding.getKonference());
				this.updateControls();
			} else {
				return;
			}
		}
	}

}
