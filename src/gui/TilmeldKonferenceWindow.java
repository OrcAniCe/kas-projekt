package gui;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

import application.model.Firma;
import application.model.Hotel;
import application.model.HotelService;
import application.model.Konference;
import application.model.Tilmelding;
import application.model.Udflugt;
import application.service.Service;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class TilmeldKonferenceWindow extends Stage {
	private Konference konference;
	private Tilmelding tilmelding;
	private DatePicker datePickAnkomstDato, datePickAfrejseDato;
	private CheckBox chbLedsager, chbFirma, chbHotel;
	private CheckBox[] chbUdflugter, chbHotelServices;
	private TextField txfLedsager, txfFirmaNavn, txfFirmaTlfNr, txfFirmaEmail;
	private Label lblUdflugter, lblOpretFirma, lblFirmaNavn, lblFirmaTlfNr, lblFirmaEmail, lblPris, lblError;
	private ListView<Hotel> lvwHoteller;
	private ListView<Firma> lvwFirmaer;
	private Button btnTilmeld;

	public TilmeldKonferenceWindow(String title, Konference konference, Tilmelding tilmelding) {

		this.konference = konference;
		this.tilmelding = tilmelding;

		this.setResizable(false);
		this.setTitle(title);
		this.setHeight(600);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		scene.getStylesheets().add(MainApp.STYLE);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		GridPane stamData = new GridPane();
		stamData.setPadding(new Insets(10));
		stamData.setHgap(10);
		stamData.setVgap(10);
		pane.add(stamData, 0, 0);

		Label lblAnkomstDato = new Label();
		lblAnkomstDato.setText("Ankomst:");
		stamData.add(lblAnkomstDato, 0, 0);

		this.datePickAnkomstDato = new DatePicker();
		this.datePickAnkomstDato.setOnAction(e -> this.updatePris());
		stamData.add(this.datePickAnkomstDato, 0, 1);

		Label lblAfrejseDato = new Label();
		lblAfrejseDato.setText("Afrejse:");
		stamData.add(lblAfrejseDato, 0, 2);

		this.datePickAfrejseDato = new DatePicker();
		this.datePickAfrejseDato.setOnAction(e -> this.updatePris());
		stamData.add(this.datePickAfrejseDato, 0, 3);

		this.chbLedsager = new CheckBox();
		this.chbLedsager.setText("Ledsager");
		this.chbLedsager.setOnAction(e -> this.selectedLedsager());
		stamData.add(this.chbLedsager, 0, 4);

		this.txfLedsager = new TextField();
		this.txfLedsager.setDisable(true);
		stamData.add(this.txfLedsager, 0, 5);

		this.lblUdflugter = new Label();
		this.lblUdflugter.setText("Udflugter:");
		this.lblUdflugter.setDisable(true);
		stamData.add(lblUdflugter, 0, 6);

		this.chbUdflugter = new CheckBox[konference.getUdflugter().size()];

		for (int i = 0; i < chbUdflugter.length; i++) {
			chbUdflugter[i] = new CheckBox();
			chbUdflugter[i].setText(this.konference.getUdflugter().get(i).getNavn() + "("
					+ this.konference.getUdflugter().get(i).getPris() + " DKK)");
			chbUdflugter[i].setDisable(true);
			chbUdflugter[i].setOnAction(e -> this.updatePris());
			stamData.add(chbUdflugter[i], 0, (7 + i));
		}

		GridPane firmaData = new GridPane();
		firmaData.setPadding(new Insets(10));
		firmaData.setHgap(10);
		firmaData.setVgap(10);
		pane.add(firmaData, 1, 0);

		this.chbFirma = new CheckBox();
		this.chbFirma.setText("Firma");
		this.chbFirma.setOnAction(e -> selectedFirma());
		firmaData.add(chbFirma, 0, 0);

		this.lvwFirmaer = new ListView<>();
		this.lvwFirmaer.setDisable(true);
		this.lvwFirmaer.setMaxHeight(200);
		firmaData.add(lvwFirmaer, 0, 1);

		this.lblOpretFirma = new Label();
		this.lblOpretFirma.setText("Mangler dit firma udfyld f�lgende:");
		firmaData.add(this.lblOpretFirma, 0, 2);

		this.lblFirmaNavn = new Label();
		this.lblFirmaNavn.setText("Navn:");
		this.lblFirmaNavn.setDisable(true);
		firmaData.add(this.lblFirmaNavn, 0, 3);

		this.txfFirmaNavn = new TextField();
		this.txfFirmaNavn.setDisable(true);
		firmaData.add(txfFirmaNavn, 0, 4);

		this.lblFirmaTlfNr = new Label();
		this.lblFirmaTlfNr.setText("Telefon nummer:");
		this.lblFirmaTlfNr.setDisable(true);
		firmaData.add(this.lblFirmaTlfNr, 0, 5);

		this.txfFirmaTlfNr = new TextField();
		this.txfFirmaTlfNr.setDisable(true);
		firmaData.add(txfFirmaTlfNr, 0, 6);

		this.lblFirmaEmail = new Label();
		this.lblFirmaEmail.setText("Email:");
		this.lblFirmaEmail.setDisable(true);
		firmaData.add(this.lblFirmaEmail, 0, 7);

		this.txfFirmaEmail = new TextField();
		txfFirmaEmail.setDisable(true);
		firmaData.add(txfFirmaEmail, 0, 8);

		GridPane hotelData = new GridPane();
		hotelData.setPadding(new Insets(10));
		hotelData.setHgap(10);
		hotelData.setVgap(10);
		pane.add(hotelData, 2, 0);

		this.chbHotel = new CheckBox();
		this.chbHotel.setText("Hotel");
		this.chbHotel.setOnAction(e -> selectedHotel());
		hotelData.add(chbHotel, 0, 0);

		this.lvwHoteller = new ListView<>();
		this.lvwHoteller.setMaxHeight(200);
		this.lvwHoteller.setDisable(true);
		this.lvwHoteller.setOnMouseClicked(e -> selectedHotelItem());
		hotelData.add(this.lvwHoteller, 0, 1);

		this.chbHotelServices = new CheckBox[this.getMaxHotelServices()];

		for (int i = 0; i < chbHotelServices.length; i++) {
			this.chbHotelServices[i] = new CheckBox();
			this.chbHotelServices[i].setVisible(false);
			this.chbHotelServices[i].setOnAction(e -> this.updatePris());
			hotelData.add(this.chbHotelServices[i], 0, (2 + i));
		}

		btnTilmeld = new Button();
		btnTilmeld.setText("Tilmeld");
		btnTilmeld.setOnAction(e -> this.tilmeldAction());
		pane.add(btnTilmeld, 2, 1);

		this.lblPris = new Label();
		pane.add(lblPris, 0, 1);

		this.lblError = new Label();
		pane.add(this.lblError, 1, 2);
		this.lblError.setStyle("-fx-text-fill: red");

		this.initControls();
	}

	private void initControls() {
		this.lvwFirmaer.getItems().setAll(Service.getFirmaer());
		this.lvwHoteller.getItems().setAll(this.konference.getHoteller());

		if (tilmelding != null) {
			this.datePickAnkomstDato.setValue(LocalDate.of(this.tilmelding.getAnkomstDato().getYear(),
					this.tilmelding.getAnkomstDato().getMonthValue(),
					this.tilmelding.getAnkomstDato().getDayOfMonth()));

			this.datePickAfrejseDato.setValue(LocalDate.of(this.tilmelding.getAfrejseDato().getYear(),
					this.tilmelding.getAfrejseDato().getMonthValue(),
					this.tilmelding.getAfrejseDato().getDayOfMonth()));
			if (tilmelding.getLedsagerNavn().isEmpty() && tilmelding.getUdflugter().size() == 0) {
				chbLedsager.setSelected(false);
			} else {
				chbLedsager.setSelected(true);
				this.lblUdflugter.setDisable(false);
				this.txfLedsager.setText(tilmelding.getLedsagerNavn());
				this.txfLedsager.setDisable(false);
				for (int i = 0; i < tilmelding.getUdflugter().size(); i++) {
					for (int j = 0; j < chbUdflugter.length; j++) {
						String navn = tilmelding.getUdflugter().get(i).getNavn() + "("
								+ tilmelding.getUdflugter().get(i).getPris() + " DKK)";
						if (navn.equals(chbUdflugter[j].getText())) {
							chbUdflugter[j].setSelected(true);
						}
						chbUdflugter[j].setDisable(false);
					}
				}
			}

			if (tilmelding.getFirma() != null) {
				this.chbFirma.setSelected(true);
				this.lvwFirmaer.setDisable(false);
				this.txfFirmaNavn.setDisable(false);
				this.txfFirmaEmail.setDisable(false);
				this.txfFirmaTlfNr.setDisable(false);
				this.lblFirmaEmail.setDisable(false);
				this.lblFirmaNavn.setDisable(false);
				this.lblFirmaTlfNr.setDisable(false);
				this.lvwFirmaer.getSelectionModel().select(tilmelding.getFirma());
			}

			if (tilmelding.getHotel() != null) {
				this.chbHotel.setSelected(true);
				this.lvwHoteller.setDisable(false);
				this.lvwHoteller.getSelectionModel().select(tilmelding.getHotel());

				this.clearCheckBoxHotelServices();
				for (int i = 0; i < tilmelding.getHotel().getServices().size(); i++) {
					chbHotelServices[i].setVisible(true);
					String serviceNavn = tilmelding.getHotel().getServices().get(i).getNavn() + "( "
							+ tilmelding.getHotel().getServices().get(i).getPris() + " DKK)";
					chbHotelServices[i].setText(serviceNavn);

					for (int j = 0; j < tilmelding.getHotelServices().size(); j++) {
						if (serviceNavn.equals(tilmelding.getHotelServices().get(j).getNavn() + "( "
								+ tilmelding.getHotel().getServices().get(j).getPris() + " DKK)")) {
							chbHotelServices[i].setSelected(true);
						}
					}

				}
			}
			btnTilmeld.setText("�ndre");
		} else {
			this.datePickAnkomstDato.setValue(LocalDate.of(this.konference.getStartDato().getYear(),
					this.konference.getStartDato().getMonthValue(), this.konference.getStartDato().getDayOfMonth()));

			this.datePickAfrejseDato.setValue(LocalDate.of(this.konference.getSlutDato().getYear(),
					this.konference.getSlutDato().getMonthValue(), this.konference.getSlutDato().getDayOfMonth()));
		}
		this.updatePris();
	}

	private void tilmeldAction() {
		int dage = this.datePickAnkomstDato.getValue().until(this.datePickAfrejseDato.getValue()).getDays();

		if (dage > 0) {
			LocalDate ankomst = this.datePickAnkomstDato.getValue();
			LocalDate afrejse = this.datePickAfrejseDato.getValue();
			try {
				ankomst = this.datePickAnkomstDato.getValue();
				afrejse = this.datePickAfrejseDato.getValue();
			} catch (Exception e) {
				this.lblError.setText("Indtast en korrekt dato");
				return;
			}

			String ledsager = "";
			ArrayList<Udflugt> udflugter = new ArrayList<>();

			if (this.chbLedsager.isSelected()) {
				if (!this.txfLedsager.getText().isEmpty()) {
					ledsager = this.txfLedsager.getText();
				} else {
					this.lblError.setText("Du skal indtaste et ledsager navn!");
					return;
				}

				for (int i = 0; i < chbUdflugter.length; i++) {
					if (chbUdflugter[i].isSelected()) {
						udflugter.add(konference.getUdflugter().get(i));
					}
				}
			}

			Firma firma = null;

			if (this.chbFirma.isSelected()) {

				if (this.txfFirmaNavn.getText().isEmpty() && this.txfFirmaEmail.getText().isEmpty()
						&& this.txfFirmaTlfNr.getText().isEmpty()) {

					Firma firmaValgt = this.lvwFirmaer.getSelectionModel().getSelectedItem();
					if (firmaValgt != null) {
						firma = firmaValgt;
					} else {
						this.lblError.setText("Du skal v�lge et firma");
						return;
					}
				} else {
					if (this.txfFirmaNavn.getText().isEmpty() || this.txfFirmaEmail.getText().isEmpty()
							|| this.txfFirmaTlfNr.getText().isEmpty()) {
						this.lblError.setText("Du skal skrive alt firma information");
						return;
					} else {
						int tlfNr;
						try {
							tlfNr = Integer.parseInt(this.txfFirmaTlfNr.getText());
						} catch (Exception e) {
							this.lblError.setText("Du skal skrive et tlf nummer");
							return;
						}
						firma = Service.createFirma(this.txfFirmaNavn.getText(), this.txfFirmaEmail.getText(), tlfNr);
					}
				}
			}

			Hotel hotel = null;
			ArrayList<HotelService> hotelServices = new ArrayList<>();

			if (this.chbHotel.isSelected()) {
				Hotel hotelValgt = this.lvwHoteller.getSelectionModel().getSelectedItem();

				if (hotelValgt != null) {
					hotel = hotelValgt;
				} else {
					this.lblError.setText("Du skal v�lge et hotel p� listen");
					return;
				}

				for (int i = 0; i < hotel.getServices().size(); i++) {
					if (this.chbHotelServices[i].isSelected()) {
						hotelServices.add(hotel.getServices().get(i));
					}
				}

			}
			if (tilmelding != null) {
				Service.updateTilmelding(tilmelding, ankomst, afrejse, ledsager, firma, hotel, udflugter,
						hotelServices);
			} else {
				Service.createTilmelding(ankomst, afrejse, ledsager, firma, Service.deltagerLoggedIn, hotel,
						this.konference, udflugter, hotelServices);
			}

			this.close();
		} else {
			this.lblError.setText("Du indtaste en ankomst dato der er mindre end din afrejse dato");
		}
	}

	public void selectedLedsager() {
		boolean selected = !this.chbLedsager.isSelected();
		for (int i = 0; i < chbUdflugter.length; i++) {
			chbUdflugter[i].setDisable(selected);
		}
		this.txfLedsager.setDisable(selected);
		this.lblUdflugter.setDisable(selected);
		this.updatePris();
	}

	public void selectedFirma() {

		boolean selected = !this.chbFirma.isSelected();

		this.lvwFirmaer.setDisable(selected);
		this.lblOpretFirma.setDisable(selected);
		this.txfFirmaNavn.setDisable(selected);
		this.txfFirmaTlfNr.setDisable(selected);
		this.txfFirmaEmail.setDisable(selected);
		this.lblFirmaNavn.setDisable(selected);
		this.lblFirmaTlfNr.setDisable(selected);
		this.lblFirmaEmail.setDisable(selected);
	}

	public void selectedHotel() {
		boolean selected = !this.chbHotel.isSelected();
		this.lvwHoteller.setDisable(selected);
		this.updatePris();
	}

	public void selectedHotelItem() {
		Hotel hotel = this.lvwHoteller.getSelectionModel().getSelectedItem();
		if (hotel != null) {
			this.updatePris();
			this.clearCheckBoxHotelServices();
			for (int i = 0; i < hotel.getServices().size(); i++) {
				chbHotelServices[i].setVisible(true);
				chbHotelServices[i].setText(
						hotel.getServices().get(i).getNavn() + "( " + hotel.getServices().get(i).getPris() + " DKK)");
			}
		}
	}

	public void updatePris() {
		int dage;
		double sum;

		if (tilmelding != null) {
			sum = tilmelding.getPrisKonference();
		} else {
			sum = konference.getPris();
		}

		try {
			dage = (int) ChronoUnit.DAYS.between(this.datePickAnkomstDato.getValue(),
					this.datePickAfrejseDato.getValue());
		} catch (Exception e) {
			return;
		}

		for (int i = 0; i < chbUdflugter.length; i++) {
			if (chbUdflugter[i].isSelected() && chbLedsager.isSelected()) {
				sum += konference.getUdflugter().get(i).getPris();
			}
		}

		Hotel hotel = this.lvwHoteller.getSelectionModel().getSelectedItem();
		if (hotel != null && chbHotel.isSelected()) {

			if (chbLedsager.isSelected()) {
				sum += hotel.getPrisDobbelt() * dage;
			} else {
				sum += hotel.getPrisEnkelt() * dage;
			}

			for (int i = 0; i < chbHotelServices.length; i++) {
				if (chbHotelServices[i].isSelected()) {
					sum += hotel.getServices().get(i).getPris() * dage;
				}
			}
		}

		this.lblPris.setText("Pris: " + sum + " DKK");
	}

	public int getMaxHotelServices() {
		int maxAntal = 0;
		for (int i = 0; i < konference.getHoteller().size(); i++) {
			if (konference.getHoteller().get(i).getServices().size() > maxAntal) {
				maxAntal = konference.getHoteller().get(i).getServices().size();
			}
		}
		return maxAntal;
	}

	public void clearCheckBoxHotelServices() {
		for (int i = 0; i < chbHotelServices.length; i++) {
			chbHotelServices[i].setSelected(false);
			chbHotelServices[i].setVisible(false);
		}
	}

}
