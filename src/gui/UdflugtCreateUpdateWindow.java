package gui;

import java.time.LocalDate;
import java.time.LocalDateTime;

import application.model.Konference;
import application.model.Udflugt;
import application.service.Service;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class UdflugtCreateUpdateWindow extends Stage {
	private Udflugt udflugt;
	private Konference konference;

	public UdflugtCreateUpdateWindow(String title, Udflugt udflugt, Konference konference) {

		this.udflugt = udflugt;
		this.konference = konference;

		this.setResizable(false);
		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		scene.getStylesheets().add(MainApp.STYLE);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private TextField txfNavn, txfBeskrivelse, txfPris;
	private DatePicker dPickStartTid, dPickSlutTid;
	private Label lblError;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		// Textfields
		txfNavn = new TextField();
		txfBeskrivelse = new TextField();
		txfPris = new TextField();

		this.dPickStartTid = new DatePicker();
		this.dPickSlutTid = new DatePicker();

		Label lblNavn = new Label();
		lblNavn.setText("Navn:");

		Label lblBeskrivelse = new Label();
		lblBeskrivelse.setText("Beskrivelse:");

		Label lblPris = new Label();
		lblPris.setText("Pris:");

		Label lblStart = new Label();
		lblStart.setText("Start:");

		Label lblSlut = new Label();
		lblSlut.setText("Slut:");

		Label lblForedragsHolder = new Label();
		lblForedragsHolder.setText("Foredragsholder:");

		pane.add(lblNavn, 0, 0);
		pane.add(lblBeskrivelse, 0, 1);
		pane.add(lblPris, 0, 2);
		pane.add(lblStart, 0, 3);
		pane.add(lblSlut, 0, 4);

		pane.add(txfNavn, 1, 0);
		pane.add(txfBeskrivelse, 1, 1);
		pane.add(txfPris, 1, 2);
		pane.add(dPickStartTid, 1, 3);
		pane.add(dPickSlutTid, 1, 4);

		Button btnLogin = new Button("OK");
		pane.add(btnLogin, 1, 5);
		GridPane.setHalignment(btnLogin, HPos.LEFT);
		btnLogin.setOnAction(event -> this.okAction());

		Button btnCreateUser = new Button("Cancel");
		pane.add(btnCreateUser, 1, 5);
		GridPane.setHalignment(btnCreateUser, HPos.RIGHT);
		btnCreateUser.setOnAction(event -> this.close());

		lblError = new Label();
		pane.add(lblError, 1, 6);
		lblError.setStyle("-fx-text-fill: red");

		this.initControls();
	}

	private void initControls() {
		this.lblError.setText("");
		if (this.udflugt != null) {
			this.txfNavn.setText(udflugt.getNavn());
			this.txfBeskrivelse.setText(udflugt.getBeskrivelse());
			this.txfPris.setText("" + this.udflugt.getPris());
			this.dPickStartTid.setValue(LocalDate.of(this.udflugt.getStartTid().getYear(),
					this.udflugt.getStartTid().getMonthValue(), this.udflugt.getStartTid().getDayOfMonth()));

			this.dPickSlutTid.setValue(LocalDate.of(this.udflugt.getSlutTid().getYear(),
					this.udflugt.getSlutTid().getMonthValue(), this.udflugt.getSlutTid().getDayOfMonth()));
		} else {
			txfNavn.clear();
			txfBeskrivelse.clear();
			txfPris.clear();
		}
	}

	private void okAction() {
		if (!this.txfNavn.getText().isEmpty() && !this.txfBeskrivelse.getText().isEmpty()) {
			Double pris = 0.0;
			try {
				pris = Double.parseDouble(this.txfPris.getText());
			} catch (Exception e) {
				this.lblError.setText("Indtast en pris");
				return;
			}
			if (this.dPickStartTid.getValue() != null && this.dPickSlutTid.getValue() != null) {
				LocalDate sl = this.dPickStartTid.getValue();
				LocalDate st = this.dPickSlutTid.getValue();
				LocalDateTime start = LocalDateTime.of(sl.getYear(), sl.getMonthValue(), sl.getDayOfMonth(), 12, 00);
				LocalDateTime slut = LocalDateTime.of(st.getYear(), st.getMonthValue(), st.getDayOfMonth(), 12, 00);

				if (this.udflugt != null) {
					Service.updateUdflugt(udflugt, this.txfNavn.getText(), this.txfBeskrivelse.getText(), pris, start,
							slut);
					this.close();
				} else {
					Service.createUdflugt(konference, this.txfNavn.getText(), this.txfBeskrivelse.getText(), pris,
							start, slut);
					this.close();
				}

			} else {
				this.lblError.setText("Indtast start/slut tid");
			}
		} else {
			this.lblError.setText("Indtast navn eller beskrivelse");
		}
	}
}
