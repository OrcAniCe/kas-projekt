package gui;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class HotelServiceCreateWindow extends Stage {
	private boolean actionOk = false;
	private String navn;
	private double pris = 0.0;

	public HotelServiceCreateWindow(String title) {

		this.setResizable(false);
		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		scene.getStylesheets().add(MainApp.STYLE);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private TextField txfNavn, txfPris;
	private Label lblError;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		// Textfields
		txfNavn = new TextField();
		txfPris = new TextField();

		Label lblNavn = new Label();
		lblNavn.setText("Navn:");

		Label lblPris = new Label();
		lblPris.setText("Pris:");

		pane.add(lblNavn, 0, 0);
		pane.add(lblPris, 0, 1);

		pane.add(txfNavn, 1, 0);
		pane.add(txfPris, 1, 1);

		Button btnCreateHotelService = new Button("Opret");
		pane.add(btnCreateHotelService, 1, 2);
		GridPane.setHalignment(btnCreateHotelService, HPos.LEFT);
		btnCreateHotelService.setOnAction(event -> this.createService());

		Button btnCancel = new Button("Cancel");
		pane.add(btnCancel, 1, 2);
		GridPane.setHalignment(btnCancel, HPos.RIGHT);
		btnCancel.setOnAction(event -> this.close());

		lblError = new Label();
		pane.add(lblError, 1, 3);
		lblError.setStyle("-fx-text-fill: red");

		this.initControls();
	}

	private void initControls() {
		this.txfNavn.clear();
		this.txfPris.clear();
	}

	public void createService() {
		if (!this.txfNavn.getText().isEmpty()) {
			try {
				this.pris = Double.parseDouble(this.txfPris.getText());
			} catch (Exception e) {
				this.lblError.setText("Skriv en pris");
				return;
			}
			this.navn = this.txfNavn.getText();
			this.actionOk = true;
			this.close();

		} else {
			this.lblError.setText("Skriv et navn");
		}
	}

	public String getNavn() {
		return this.navn;
	}

	public double getPris() {
		return this.pris;
	}

	public boolean isAction() {
		return this.actionOk;
	}
}
