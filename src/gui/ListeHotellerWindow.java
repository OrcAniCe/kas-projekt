package gui;

import application.model.Konference;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class ListeHotellerWindow extends Stage {
	private Konference konference;
	private TextArea txtInfo;

	public ListeHotellerWindow(String title, Konference konference) {
		this.konference = konference;

		this.setResizable(false);
		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		scene.getStylesheets().add(MainApp.STYLE);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		this.txtInfo = new TextArea();
		this.txtInfo.setMaxSize(500, 500);
		this.txtInfo.setMinSize(500, 500);
		pane.add(txtInfo, 0, 0);
		Button btnOk = new Button("OK");
		pane.add(btnOk, 0, 1);
		GridPane.setHalignment(btnOk, HPos.LEFT);
		btnOk.setOnAction(event -> this.okAction());

		this.initControls();
	}

	private void initControls() {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < konference.getHoteller().size(); i++) {
			sb.append(konference.getHoteller().get(i).getNavn() + ":\n");
			for (int j = 0; j < konference.getTilmeldeinger().size(); j++) {
				if (konference.getTilmeldeinger().get(j).getHotel() != null) {
					if (konference.getTilmeldeinger().get(j).getHotel().getNavn()
							.equals(konference.getHoteller().get(i).getNavn())) {
						sb.append("  " + konference.getTilmeldeinger().get(j).getDeltager().getNavn() + "\n");
						for (int j2 = 0; j2 < konference.getTilmeldeinger().get(j).getHotelServices().size(); j2++) {
							sb.append("    - "
									+ konference.getTilmeldeinger().get(j).getHotelServices().get(j2).getNavn() + "\n");
						}
					}
				}
			}
			sb.append("\n");
		}

		this.txtInfo.setText(sb.toString());
	}

	private void okAction() {
		this.close();
	}

}
