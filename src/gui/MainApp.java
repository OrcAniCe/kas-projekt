package gui;

import application.model.Deltager;
import application.service.Service;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainApp extends Application {
	public static final String STYLE = "/gui/style.css";
	private Stage primaryStage;
	private Label lblLoginInformation;
	private LoginWindow loginWindow;
	private TabPane tabPane;
	private KonferenceListePane KonferenceListePane;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void init() {
		Service.initStorage();
	}

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		primaryStage.setTitle("KAS");
		primaryStage.setWidth(800);
		primaryStage.setHeight(800);
		primaryStage.setResizable(false);
		GridPane pane = new GridPane();
		pane.setMinSize(600, 600);
		pane.setGridLinesVisible(false);

		this.loginWindow = new LoginWindow("Login", this.primaryStage, this);
		loginWindow.showAndWait();

		this.initContent(pane);

		Scene scene = new Scene(pane);
		scene.getStylesheets().add(MainApp.STYLE);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	private void initContent(GridPane pane) {

		// GridPane der indeholder Label med info, button med indstillinger og
		// button med logud.
		GridPane paneLoginInformation = new GridPane();
		paneLoginInformation.setPadding(new Insets(10));
		paneLoginInformation.setHgap(10);
		paneLoginInformation.setVgap(10);
		paneLoginInformation.setGridLinesVisible(false);

		this.lblLoginInformation = new Label();
		Button btnIndstillinger = new Button("Indstillinger");
		btnIndstillinger.setOnAction(e -> this.indstillingerAction());
		Button btnLogud = new Button("Log ud");
		btnLogud.setOnAction(e -> this.logudAction());

		paneLoginInformation.add(lblLoginInformation, 0, 0);
		paneLoginInformation.add(btnIndstillinger, 1, 0);
		paneLoginInformation.add(btnLogud, 2, 0);

		//
		this.tabPane = new TabPane();
		tabPane.setPrefSize(this.primaryStage.getWidth(), this.primaryStage.getHeight());
		this.initTabPane(tabPane);

		pane.add(tabPane, 0, 1);
		pane.add(paneLoginInformation, 0, 0);

		this.updateLoginInformation();
	}

	private void initTabPane(TabPane tabPane) {
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		// Tab Tilmeld konference
		Tab tabKonferenceListe = new Tab("Konference liste");
		tabPane.getTabs().add(tabKonferenceListe);

		KonferenceListePane = new KonferenceListePane();
		tabKonferenceListe.setContent(KonferenceListePane);
		tabKonferenceListe.setOnSelectionChanged(event -> KonferenceListePane.updateControls());
		KonferenceListePane.updateControls();

		// Tab Tilmeldte konferencer
		Tab tabTilmeldteKonferencer = new Tab("Tilmeldte konferencer");
		tabPane.getTabs().add(tabTilmeldteKonferencer);

		TilmeldteKonferencerPane tilmeldteKonferencerPane = new TilmeldteKonferencerPane();
		tabTilmeldteKonferencer.setContent(tilmeldteKonferencerPane);
		tabTilmeldteKonferencer.setOnSelectionChanged(event -> tilmeldteKonferencerPane.updateControls());
	}

	public void updateLoginInformation() {
		Deltager deltager = Service.deltagerLoggedIn;

		// S�tter label lblLoginInformation
		if (deltager != null) {
			this.lblLoginInformation.setText("Velkommen " + deltager.getNavn());
		}

		// Bestemmer om tab administrator skal vises
		if (deltager.isAdministrator()) {
			if (this.tabPane.getTabs().size() <= 2) {

				// Tabs Administration
				Tab tabkonferencer = new Tab("Konferencer");
				this.tabPane.getTabs().add(tabkonferencer);

				KonferencerPane KonferencerPane = new KonferencerPane();
				tabkonferencer.setContent(KonferencerPane);
				tabkonferencer.setOnSelectionChanged(event -> KonferencerPane.updateControls());

			}
		} else {

			// Fjerner administrations tabs hvis deltageren ikke er
			// administrator
			for (int i = 2; i < this.tabPane.getTabs().size(); i++) {
				this.tabPane.getTabs().remove(i);
			}
		}

		// Ved login skal f�rste tab vises
		this.tabPane.getSelectionModel().select(0);
		KonferenceListePane.updateControls();
	}

	private void indstillingerAction() {
		DeltagerCreateUpdateWindow updateDeltager = new DeltagerCreateUpdateWindow("Opdatere",
				Service.deltagerLoggedIn);
		updateDeltager.initModality(Modality.APPLICATION_MODAL);
		updateDeltager.showAndWait();
		this.updateLoginInformation();
	}

	private void logudAction() {
		Service.logud();
		this.primaryStage.close();
		this.loginWindow.show();
	}
}
