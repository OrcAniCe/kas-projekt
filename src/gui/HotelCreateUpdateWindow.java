package gui;

import java.util.ArrayList;

import application.model.Hotel;
import application.model.HotelService;
import application.model.Konference;
import application.service.Service;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class HotelCreateUpdateWindow extends Stage {
	private Hotel hotel;
	private Konference konference;

	public HotelCreateUpdateWindow(String title, Hotel hotel, Konference konference) {

		this.hotel = hotel;
		this.konference = konference;

		this.setResizable(false);
		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		scene.getStylesheets().add(MainApp.STYLE);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private TextField txfNavn, txfTlfNr, txfAdresse, txfPrisEnkelt, txfPrisDobbelt;
	private ListView<HotelService> lvwServices;
	private Label lblError;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		// Textfields
		txfNavn = new TextField();
		txfTlfNr = new TextField();
		txfAdresse = new TextField();
		txfPrisEnkelt = new TextField();
		txfPrisDobbelt = new TextField();
		this.lvwServices = new ListView<>();
		this.lvwServices.setMaxHeight(130);

		Label lblNavn = new Label();
		lblNavn.setText("Navn:");

		Label lblTlfNr = new Label();
		lblTlfNr.setText("Tlf nr.:");

		Label lblAdresse = new Label();
		lblAdresse.setText("Adresse:");

		Label lblPrisEnkelt = new Label();
		lblPrisEnkelt.setText("Enkelt v�r.:");

		Label lblPrisDobbelt = new Label();
		lblPrisDobbelt.setText("Dobbelt v�r.:");

		Label lblServices = new Label();
		lblServices.setText("Services:");

		pane.add(lblNavn, 0, 0);
		pane.add(lblTlfNr, 0, 1);
		pane.add(lblAdresse, 0, 2);
		pane.add(lblPrisEnkelt, 0, 3);
		pane.add(lblPrisDobbelt, 0, 4);
		pane.add(lblServices, 2, 0);

		pane.add(txfNavn, 1, 0);
		pane.add(txfTlfNr, 1, 1);
		pane.add(txfAdresse, 1, 2);
		pane.add(txfPrisEnkelt, 1, 3);
		pane.add(txfPrisDobbelt, 1, 4);
		pane.add(lvwServices, 2, 1, 1, 4);

		Button btnCreateService = new Button("Opret");
		pane.add(btnCreateService, 2, 5);
		GridPane.setHalignment(btnCreateService, HPos.LEFT);
		btnCreateService.setOnAction(event -> this.createService());

		Button btnDeleteService = new Button("Slet");
		pane.add(btnDeleteService, 2, 5);
		GridPane.setHalignment(btnDeleteService, HPos.RIGHT);
		btnDeleteService.setOnAction(event -> this.deleteService());

		Button btnOK = new Button("OK");
		pane.add(btnOK, 1, 5);
		GridPane.setHalignment(btnOK, HPos.LEFT);
		btnOK.setOnAction(event -> this.okAction());

		Button btnCreateUser = new Button("Cancel");
		pane.add(btnCreateUser, 1, 5);
		GridPane.setHalignment(btnCreateUser, HPos.RIGHT);
		btnCreateUser.setOnAction(event -> this.close());

		lblError = new Label();
		pane.add(lblError, 1, 6);
		lblError.setStyle("-fx-text-fill: red");

		this.initControls();
	}

	private void initControls() {
		this.lblError.setText("");
		if (this.hotel != null) {
			this.txfNavn.setText(hotel.getNavn());
			this.txfTlfNr.setText("" + hotel.getTlfNr());
			this.txfAdresse.setText(hotel.getAdresse());
			this.txfPrisEnkelt.setText("" + hotel.getPrisEnkelt());
			this.txfPrisDobbelt.setText("" + hotel.getPrisDobbelt());
			if (hotel.getServices() != null) {
				this.lvwServices.getItems().addAll(hotel.getServices());
			}

		} else {
			this.txfNavn.clear();
			this.txfTlfNr.clear();
			this.txfAdresse.clear();
			this.txfPrisEnkelt.clear();
			this.txfPrisDobbelt.clear();
			this.lvwServices.getItems().clear();
		}
	}

	private void okAction() {
		if (!this.txfNavn.getText().isEmpty() && !this.txfAdresse.getText().isEmpty()) {
			double prisEnkelt, prisDobbelt;
			int tlfNr;
			try {
				tlfNr = Integer.parseInt(txfTlfNr.getText());
				prisEnkelt = Double.parseDouble(txfPrisEnkelt.getText());
				prisDobbelt = Double.parseDouble(txfPrisDobbelt.getText());
			} catch (Exception e) {
				this.lblError.setText("Skriv pris og telefon nummer");
				return;
			}
			ArrayList<HotelService> hotelService = new ArrayList<>();

			for (int i = 0; i < this.lvwServices.getItems().size(); i++) {
				hotelService.add(this.lvwServices.getItems().get(i));
			}

			if (this.hotel != null) {
				Service.updateHotel(hotel, this.txfNavn.getText(), tlfNr, this.txfAdresse.getText(), prisEnkelt,
						prisDobbelt, hotelService);
				this.hide();
			} else {
				Service.createHotel(konference, this.txfNavn.getText(), tlfNr, this.txfAdresse.getText(), prisEnkelt,
						prisDobbelt, hotelService);
				this.hide();
			}

		} else {
			this.lblError.setText("Skriv et navn eller en adresse");
		}
	}

	public void deleteService() {
		HotelService hotelService = this.lvwServices.getSelectionModel().getSelectedItem();
		if (hotelService != null) {
			this.lvwServices.getItems().remove(hotelService);
		}
	}

	public void createService() {
		HotelServiceCreateWindow hotelServiceCreateWindow = new HotelServiceCreateWindow("Opret service");
		hotelServiceCreateWindow.showAndWait();
		if (hotelServiceCreateWindow.isAction()) {
			this.lvwServices.getItems().add(
					Service.createHotelService(hotelServiceCreateWindow.getNavn(), hotelServiceCreateWindow.getPris()));
		}
	}
}
