package gui;

import java.time.LocalDate;
import java.time.LocalDateTime;

import application.model.Konference;
import application.model.Tilstand;
import application.service.Service;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class KonferenceCreateUpdateWindow extends Stage {
	private Konference konference;

	public KonferenceCreateUpdateWindow(String title, Konference konference) {

		this.konference = konference;

		this.setResizable(false);
		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		scene.getStylesheets().add(MainApp.STYLE);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private TextField txfLand, txfPostNr, txfBy, txfVejNavn, txfVejNr, txfEtage, txfTitel, txfPris, txfTlfNr,
			txfMaxDeltagere;
	private DatePicker txfStartDato, txfSlutDato;
	private CheckBox chbAaben;
	private Label lblError;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		// Labels
		Label[] labels = new Label[12];
		String[] labelNavne = { "Titel:", "Pris:", "Tlf. nr.:", "Max Deltagere:", "Start dato:", "Slut dato:", "Land:",
				"Post nr.:", "By:", "Vej navn:", "Vej nr.:", "Etage:" };

		for (int i = 0; i < labelNavne.length; i++) {
			labels[i] = new Label(labelNavne[i]);
			pane.add(labels[i], 0, i);
		}

		// Textfields
		txfTitel = new TextField();
		txfPris = new TextField();
		txfTlfNr = new TextField();
		txfMaxDeltagere = new TextField();
		txfStartDato = new DatePicker();
		txfSlutDato = new DatePicker();
		txfLand = new TextField();
		txfPostNr = new TextField();
		txfBy = new TextField();
		txfVejNavn = new TextField();
		txfVejNr = new TextField();
		txfEtage = new TextField();

		this.chbAaben = new CheckBox();
		this.chbAaben.setText("�ben");
		this.chbAaben.setVisible(false);

		pane.add(txfTitel, 1, 0);
		pane.add(txfPris, 1, 1);
		pane.add(txfTlfNr, 1, 2);
		pane.add(txfMaxDeltagere, 1, 3);
		pane.add(txfStartDato, 1, 4);
		pane.add(txfSlutDato, 1, 5);
		pane.add(txfLand, 1, 6);
		pane.add(txfPostNr, 1, 7);
		pane.add(txfBy, 1, 8);
		pane.add(txfVejNavn, 1, 9);
		pane.add(txfVejNr, 1, 10);
		pane.add(txfEtage, 1, 11);
		pane.add(chbAaben, 1, 12);

		Button btnOK = new Button("OK");
		pane.add(btnOK, 1, 13);
		GridPane.setHalignment(btnOK, HPos.LEFT);
		btnOK.setOnAction(event -> this.okAction());

		Button btnCancel = new Button("Cancel");
		pane.add(btnCancel, 1, 13);
		GridPane.setHalignment(btnCancel, HPos.RIGHT);
		btnCancel.setOnAction(event -> this.close());

		lblError = new Label();
		pane.add(lblError, 1, 14);
		lblError.setStyle("-fx-text-fill: red");

		this.initControls();
	}

	private void initControls() {
		if (this.konference != null) {
			txfStartDato.setValue(LocalDate.of(this.konference.getStartDato().getYear(),
					this.konference.getStartDato().getMonthValue(), this.konference.getStartDato().getDayOfMonth()));

			txfSlutDato.setValue(LocalDate.of(this.konference.getSlutDato().getYear(),
					this.konference.getSlutDato().getMonthValue(), this.konference.getSlutDato().getDayOfMonth()));
			txfLand.setText(konference.getLand());
			txfPostNr.setText("" + konference.getPostNr());
			txfBy.setText(konference.getBy());
			txfVejNavn.setText(konference.getVejNavn());
			txfVejNr.setText(konference.getVejNr());
			txfEtage.setText(konference.getEtage());
			txfTitel.setText(konference.getTitel());
			txfPris.setText("" + konference.getPris());
			txfTlfNr.setText("" + konference.getTlfNr());
			txfMaxDeltagere.setText("" + konference.getMaxDeltagere());
			chbAaben.setVisible(true);

			if (konference.getTilstand() == Tilstand.LUKKET || konference.getTilstand() == Tilstand.UDARBEJDES) {
				chbAaben.setSelected(false);
			} else {
				chbAaben.setSelected(true);
			}

		} else {
			txfLand.clear();
			txfPostNr.clear();
			txfBy.clear();
			txfVejNavn.clear();
			txfVejNr.clear();
			txfEtage.clear();
			txfTitel.clear();
			txfPris.clear();
			txfTlfNr.clear();
			txfMaxDeltagere.clear();
			chbAaben.setVisible(false);
		}
	}

	// -------------------------------------------------------------------------

	private void okAction() {
		if (!txfTitel.getText().isEmpty()) {
			double pris;
			int tlfNr;
			int maxDeltagere;
			try {
				pris = Double.parseDouble(txfPris.getText());
				tlfNr = Integer.parseInt(txfTlfNr.getText());
				maxDeltagere = Integer.parseInt(txfMaxDeltagere.getText());
			} catch (Exception e) {
				lblError.setText("Skriv en pris/telefonnummer/max deltagere");
				return;
			}

			LocalDate startDato;
			LocalDate slutDato;
			try {
				startDato = txfStartDato.getValue();
				slutDato = txfStartDato.getValue();

				int dage = startDato.until(slutDato).getDays();

				if (dage < 0) {
					lblError.setText("Angiv en slut dato der er mindre end start datoen");
					return;
				}

			} catch (Exception e) {
				lblError.setText("Angiv start og slut tidspunkt");
				return;
			}

			if (!txfLand.getText().isEmpty() && !txfBy.getText().isEmpty()) {
				if (!txfVejNavn.getText().isEmpty() && !txfVejNr.getText().isEmpty()) {

					int postNr;
					try {
						postNr = Integer.parseInt(txfPostNr.getText());
					} catch (Exception e) {
						lblError.setText("Angiv post nummer");
						return;
					}
					LocalDateTime startTid = LocalDateTime.of(startDato.getYear(), startDato.getMonthValue(),
							startDato.getDayOfMonth(), 12, 00);
					LocalDateTime slutTid = LocalDateTime.of(slutDato.getYear(), slutDato.getMonthValue(),
							slutDato.getDayOfMonth(), 12, 00);
					if (konference == null) {
						// create new
						Service.createKonference(txfLand.getText(), postNr, txfBy.getText(), txfVejNavn.getText(),
								txfVejNr.getText(), txfEtage.getText(), txfTitel.getText(), pris, maxDeltagere,
								startTid, slutTid, tlfNr);
						this.close();
					} else {
						// update
						Tilstand tilstand;
						if (this.chbAaben.isSelected()) {
							tilstand = Tilstand.ÅBEN;
						} else {
							tilstand = Tilstand.LUKKET;
						}

						Service.updateKonference(konference, txfLand.getText(), postNr, txfBy.getText(),
								txfVejNavn.getText(), txfVejNr.getText(), txfEtage.getText(), txfTitel.getText(), pris,
								maxDeltagere, startTid, slutTid, tlfNr, tilstand);
						this.close();
					}

				} else {
					lblError.setText("Angiv vej navn og vej nr");
				}
			} else {
				lblError.setText("Angiv land og by");
			}

		} else {
			lblError.setText("Skriv en titel");
		}
	}

}
