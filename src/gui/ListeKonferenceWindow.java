package gui;

import application.model.Konference;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class ListeKonferenceWindow extends Stage {
	private Konference konference;
	private TextArea txtInfo;

	public ListeKonferenceWindow(String title, Konference konference) {
		this.konference = konference;

		this.setResizable(false);
		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		scene.getStylesheets().add(MainApp.STYLE);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		this.txtInfo = new TextArea();
		this.txtInfo.setMaxSize(500, 500);
		this.txtInfo.setMinSize(500, 500);
		pane.add(txtInfo, 0, 0);
		Button btnLogin = new Button("OK");
		pane.add(btnLogin, 0, 1);
		GridPane.setHalignment(btnLogin, HPos.LEFT);
		btnLogin.setOnAction(event -> this.okAction());

		this.initControls();
	}

	private void initControls() {
		StringBuilder sb = new StringBuilder();
		int deltager = 0;
		if (konference.getTilmeldeinger().size() != 0) {
			sb.append("Deltagere:\n\n");
		}

		for (int i = 0; i < konference.getTilmeldeinger().size(); i++) {
			sb.append(konference.getTilmeldeinger().get(i).getDeltager().getNavn() + " - "
					+ konference.getTilmeldeinger().get(i).getDeltager().getEmail() + " - ("
					+ konference.getTilmeldeinger().get(i).getPrisTotal() + " DKK)\n");
			deltager++;
		}

		if (deltager != 0) {
			sb.append("\nAntal: " + deltager);
		}
		this.txtInfo.setText(sb.toString());
	}

	private void okAction() {
		this.close();
	}

}
