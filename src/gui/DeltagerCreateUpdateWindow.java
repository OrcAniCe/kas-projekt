package gui;

import application.model.Deltager;
import application.service.Service;
import application.service.Validate;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class DeltagerCreateUpdateWindow extends Stage {
	private Deltager deltager;

	public DeltagerCreateUpdateWindow(String title, Deltager deltager) {

		this.deltager = deltager;

		this.setResizable(false);
		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		scene.getStylesheets().add(MainApp.STYLE);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	private TextField txfLand, txfPostNr, txfBy, txfVejNavn, txfVejNr, txfEtage, txfNavn, txfEmail, txfTlfNr;
	private PasswordField txfPassword;
	private Label lblError;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		// Labels
		Label[] labels = new Label[10];
		String[] labelNavne = { "Navn: *", "Email: *", "Tlf. nr.: *", "Password: *", "Land", "Post nr.", "By",
				"Vej navn", "Vej nr.", "Etage" };

		for (int i = 0; i < labelNavne.length; i++) {
			labels[i] = new Label(labelNavne[i]);
			pane.add(labels[i], 0, i);
		}

		// Textfields
		txfNavn = new TextField();
		txfEmail = new TextField();
		txfTlfNr = new TextField();
		txfPassword = new PasswordField();
		txfLand = new TextField();
		txfPostNr = new TextField();
		txfBy = new TextField();
		txfVejNavn = new TextField();
		txfVejNr = new TextField();
		txfEtage = new TextField();

		pane.add(txfNavn, 1, 0);
		pane.add(txfEmail, 1, 1);
		pane.add(txfTlfNr, 1, 2);
		pane.add(txfPassword, 1, 3);
		pane.add(txfLand, 1, 4);
		pane.add(txfPostNr, 1, 5);
		pane.add(txfBy, 1, 6);
		pane.add(txfVejNavn, 1, 7);
		pane.add(txfVejNr, 1, 8);
		pane.add(txfEtage, 1, 9);

		Button btnLogin = new Button("OK");
		pane.add(btnLogin, 1, 10);
		GridPane.setHalignment(btnLogin, HPos.LEFT);
		btnLogin.setOnAction(event -> this.okAction());

		Button btnCreateUser = new Button("Cancel");
		pane.add(btnCreateUser, 1, 10);
		GridPane.setHalignment(btnCreateUser, HPos.RIGHT);
		btnCreateUser.setOnAction(event -> this.close());

		lblError = new Label();
		pane.add(lblError, 1, 11);
		lblError.setStyle("-fx-text-fill: red");

		this.initControls();
	}

	private void initControls() {
		if (this.deltager != null) {
			txfLand.setText(deltager.getLand());
			if (deltager.getPostNr() == -1) {
				txfPostNr.setText("");
			} else {
				txfPostNr.setText("" + deltager.getPostNr());
			}
			txfBy.setText(deltager.getBy());
			txfVejNavn.setText(deltager.getVejNavn());
			txfVejNr.setText(deltager.getVejNr());
			txfEtage.setText(deltager.getEtage());
			txfNavn.setText(deltager.getNavn());
			txfEmail.setText(deltager.getEmail());
			txfTlfNr.setText("" + deltager.getTlfNr());
			txfPassword.setText(deltager.getPassword());
		} else {
			txfLand.clear();
			txfPostNr.clear();
			txfBy.clear();
			txfVejNavn.clear();
			txfVejNr.clear();
			txfEtage.clear();
			txfNavn.clear();
			txfEmail.clear();
			txfTlfNr.clear();
			txfPassword.clear();
		}
	}

	// -------------------------------------------------------------------------

	private void okAction() {
		if (!Validate.isValidName(txfNavn.getText())) {
			this.lblError.setText("Angiv et navn");
			return;
		}
		if (!Validate.isValidEmail(txfEmail.getText())) {
			this.lblError.setText("Angiv en email");
			return;
		}

		// TJEK OM BRUGEREN FINDES ALLEREDE!

		int tlfNr = -1;
		try {
			tlfNr = Integer.parseInt(this.txfTlfNr.getText());
		} catch (Exception e) {
			this.lblError.setText("Skriv et tal i Tlf. nr.");
			return;
		}

		if (this.txfPassword.getText().length() >= 4 && !this.txfPassword.getText().isEmpty()) {

		} else {
			this.lblError.setText("Skriv et password");
			return;
		}

		int postNr = -1;
		try {
			if (!txfPostNr.getText().isEmpty()) {
				postNr = Integer.parseInt(txfPostNr.getText());
			}
		} catch (Exception e) {
			this.lblError.setText("Skriv et tal i post nr.");
			return;
		}

		if (this.deltager != null) {
			Service.updateDeltager(deltager, this.txfLand.getText(), postNr, this.txfBy.getText(),
					this.txfVejNavn.getText(), this.txfVejNr.getText(), this.txfEtage.getText(), tlfNr,
					this.txfNavn.getText(), this.txfEmail.getText(), this.txfPassword.getText(),
					this.deltager.isAdministrator());
			this.close();
		} else {
			Service.createDeltager(this.txfLand.getText(), postNr, this.txfBy.getText(), this.txfVejNavn.getText(),
					this.txfVejNr.getText(), this.txfEtage.getText(), tlfNr, this.txfNavn.getText(),
					this.txfEmail.getText(), this.txfPassword.getText(), false);
			this.close();
		}
	}
}
