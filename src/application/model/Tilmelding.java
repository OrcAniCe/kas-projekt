package application.model;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class Tilmelding {
	private LocalDate ankomstDato;
	private LocalDate afrejseDato;
	private String ledsagerNavn;
	private Firma firma;
	private Deltager deltager;
	private Hotel hotel;
	private Konference konference;
	private ArrayList<Udflugt> udflugter;
	private ArrayList<HotelService> hotelServices;

	public Tilmelding(LocalDate ankomstDato, LocalDate afrejseDato, String ledsagerNavn, Firma firma, Deltager deltager,
			Hotel hotel, Konference konference, ArrayList<Udflugt> udflugter, ArrayList<HotelService> hotelServices) {
		this.ankomstDato = ankomstDato;
		this.afrejseDato = afrejseDato;
		this.ledsagerNavn = ledsagerNavn;
		this.firma = firma;
		this.deltager = deltager;
		this.hotel = hotel;
		this.konference = konference;
		this.udflugter = udflugter;
		this.hotelServices = hotelServices;
	}

	public LocalDate getAnkomstDato() {
		return ankomstDato;
	}

	public void setAnkomstDato(LocalDate ankomstDato) {
		this.ankomstDato = ankomstDato;
	}

	public LocalDate getAfrejseDato() {
		return afrejseDato;
	}

	public void setAfrejseDato(LocalDate afrejseDato) {
		this.afrejseDato = afrejseDato;
	}

	public String getLedsagerNavn() {
		return ledsagerNavn;
	}

	public void setLedsagerNavn(String ledsagerNavn) {
		this.ledsagerNavn = ledsagerNavn;
	}

	public Firma getFirma() {
		return firma;
	}

	public void setFirma(Firma firma) {
		this.firma = firma;
	}

	public Deltager getDeltager() {
		return deltager;
	}

	public void setDeltager(Deltager deltager) {
		this.deltager = deltager;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public Konference getKonference() {
		return konference;
	}

	public void setKonference(Konference konference) {
		this.konference = konference;
	}

	public ArrayList<Udflugt> getUdflugter() {
		return udflugter;
	}

	public void setUdflugter(ArrayList<Udflugt> udflugter) {
		this.udflugter = udflugter;
	}

	public ArrayList<HotelService> getHotelServices() {
		return hotelServices;
	}

	public void setHotelServices(ArrayList<HotelService> hotelServices) {
		this.hotelServices = hotelServices;
	}

	public double getPrisUdflugter() {
		double sum = 0.0;
		for (int i = 0; i < this.udflugter.size(); i++) {
			sum += this.udflugter.get(i).getPris();
		}
		return sum;
	}

	public double getPrisHotel() {
		if (this.hotel != null) {
			if (this.ledsagerNavn.isEmpty() || this.ledsagerNavn == "") {
				return this.hotel.getPrisEnkelt() * this.getDage();
			} else {
				return this.hotel.getPrisDobbelt() * this.getDage();
			}
		} else {
			return 0.0;
		}
	}

	public double getPrisKonference() {
		for (int i = 0; i < this.konference.getForedrag().size(); i++) {
			for (int j = 0; j < this.konference.getForedrag().get(i).getForedragsholder().size(); j++) {
				if (this.konference.getForedrag().get(i).getForedragsholder().get(j).equals(this.deltager)) {
					return 0.0;
				}
			}
		}
		return this.konference.getPris();
	}

	public double getPrisHotelServices() {
		double sum = 0.0;
		for (int i = 0; i < this.hotelServices.size(); i++) {
			sum += this.hotelServices.get(i).getPris() * this.getDage();
		}
		return sum;
	}

	public double getPrisTotal() {
		double totalPris = this.getPrisHotel() + this.getPrisHotelServices() + this.getPrisKonference()
				+ getPrisUdflugter();
		return totalPris;
	}

	public int getDage() {
		long dage = ChronoUnit.DAYS.between(ankomstDato, afrejseDato);
		return (int) dage;
	}

	@Override
	public String toString() {
		return konference.getTitel() + " - " + konference.getTilstand();
	}

}
