package application.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Udflugt {
	private String navn;
	private String beskrivelse;
	private double pris;
	private LocalDateTime startTid;
	private LocalDateTime slutTid;
	private ArrayList<Tilmelding> tilmeldinger;

	public Udflugt(String navn, String beskrivelse, double pris, LocalDateTime startTid, LocalDateTime slutTid) {
		this.navn = navn;
		this.beskrivelse = beskrivelse;
		this.pris = pris;
		this.startTid = startTid;
		this.slutTid = slutTid;
		this.tilmeldinger = new ArrayList<>();
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	public double getPris() {
		return pris;
	}

	public void setPris(double pris) {
		this.pris = pris;
	}

	public LocalDateTime getStartTid() {
		return startTid;
	}

	public void setStartTid(LocalDateTime startTid) {
		this.startTid = startTid;
	}

	public LocalDateTime getSlutTid() {
		return slutTid;
	}

	public void setSlutTid(LocalDateTime slutTid) {
		this.slutTid = slutTid;
	}

	public ArrayList<Tilmelding> getTilmeldinger() {
		return tilmeldinger;
	}

	public void deleteTilmelding(Tilmelding tilmelding) {
		this.tilmeldinger.remove(tilmelding);
	}

	public void addTilmelding(Tilmelding tilmelding) {
		this.tilmeldinger.add(tilmelding);
	}

	@Override
	public String toString() {
		return navn + " | " + startTid + " - " + slutTid + " (" + pris + " DKK)";
	}

}
