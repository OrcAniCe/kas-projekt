package application.model;

import java.util.ArrayList;

public class Hotel {
	private String navn;
	private int tlfNr;
	private String adresse;
	private double prisEnkelt;
	private double prisDobbelt;
	private ArrayList<Tilmelding> tilmeldinger;
	private ArrayList<HotelService> services;

	public Hotel(String navn, int tlfNr, String adresse, double prisEnkelt, double prisDobbelt,
			ArrayList<HotelService> services) {
		this.navn = navn;
		this.tlfNr = tlfNr;
		this.adresse = adresse;
		this.prisEnkelt = prisEnkelt;
		this.prisDobbelt = prisDobbelt;
		this.services = services;
		this.tilmeldinger = new ArrayList<>();
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public int getTlfNr() {
		return tlfNr;
	}

	public void setTlfNr(int tlfNr) {
		this.tlfNr = tlfNr;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public double getPrisEnkelt() {
		return prisEnkelt;
	}

	public void setPrisEnkelt(double prisEnkelt) {
		this.prisEnkelt = prisEnkelt;
	}

	public double getPrisDobbelt() {
		return prisDobbelt;
	}

	public void setPrisDobbelt(double prisDobbelt) {
		this.prisDobbelt = prisDobbelt;
	}

	public ArrayList<Tilmelding> getTilmeldinger() {
		return tilmeldinger;
	}

	public void setTilmeldinger(ArrayList<Tilmelding> tilmeldinger) {
		this.tilmeldinger = tilmeldinger;
	}

	public ArrayList<HotelService> getServices() {
		return services;
	}

	public void setServices(ArrayList<HotelService> services) {
		this.services = services;
	}

	public void addService(HotelService service) {
		this.services.add(service);
	}

	public void deleteService(HotelService service) {
		this.services.remove(service);
	}

	public void addTilmelding(Tilmelding tilmelding) {
		this.tilmeldinger.add(tilmelding);
	}

	public void deleteTilmelding(Tilmelding tilmelding) {
		this.tilmeldinger.remove(tilmelding);
	}

	@Override
	public String toString() {
		return navn + " Enkelt: " + prisEnkelt + "DKK - Dobbelt: " + prisDobbelt + "DKK";
	}

}
