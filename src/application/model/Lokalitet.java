package application.model;

public class Lokalitet {
private String land;
private int postNr;
private String by;
private String vejNavn;
private String vejNr;
private String etage;

public Lokalitet(String land, int postNr, String by, String vejNavn, String vejNr, String etage) {
	this.land = land;
	this.postNr = postNr;
	this.by = by;
	this.vejNavn = vejNavn;
	this.vejNr = vejNr;
	this.etage = etage;
}

public String getLand() {
	return this.land;
}

public void setLand(String land) {
	this.land = land;
}

public int getPostNr() {
	return this.postNr;
}

public void setPostNr(int postNr) {
	this.postNr = postNr;
}

public String getBy() {
	return by;
}

public void setBy(String by) {
	this.by = by;
}

public String getVejNavn() {
	return this.vejNavn;
}

public void setVejNavn(String vejNavn) {
	this.vejNavn = vejNavn;
}

public String getVejNr() {
	return this.vejNr;
}

public void setVejNr(String vejNr) {
	this.vejNr = vejNr;
}

public String getEtage() {
	return this.etage;
}

public void setEtage(String etage) {
	this.etage = etage;
}


}
