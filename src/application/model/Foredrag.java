package application.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Foredrag {
	private String titel;
	private String beskrivelse;
	private LocalDateTime startTid;
	private LocalDateTime slutTid;
	private ArrayList<Deltager> foredragsholder;

	public Foredrag(String titel, String beskrivelse, LocalDateTime startTid, LocalDateTime slutTid,
			ArrayList<Deltager> foredragsholder) {
		this.titel = titel;
		this.beskrivelse = beskrivelse;
		this.startTid = startTid;
		this.slutTid = slutTid;
		this.foredragsholder = foredragsholder;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	public LocalDateTime getStartTid() {
		return startTid;
	}

	public void setStartTid(LocalDateTime startTid) {
		this.startTid = startTid;
	}

	public LocalDateTime getSlutTid() {
		return slutTid;
	}

	public void setSlutTid(LocalDateTime slutTid) {
		this.slutTid = slutTid;
	}

	public ArrayList<Deltager> getForedragsholder() {
		return foredragsholder;
	}

	public void setForedragsholder(ArrayList<Deltager> foredragsholder) {
		this.foredragsholder = foredragsholder;
	}

	@Override
	public String toString() {
		return titel + " - " + startTid + " - " + slutTid;
	}

}
