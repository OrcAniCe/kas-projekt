package application.model;

public class Deltager extends Lokalitet {
	private int tlfNr;
	private String navn;
	private String email;
	private String password;
	private boolean administrator;

	public Deltager(String land, int postNr, String by, String vejNavn, String vejNr, String etage, int tlfNr,
			String navn, String email, String password, boolean administrator) {
		super(land, postNr, by, vejNavn, vejNr, etage);
		this.tlfNr = tlfNr;
		this.navn = navn;
		this.email = email;
		this.password = password;
		this.administrator = administrator;
	}

	public int getTlfNr() {
		return tlfNr;
	}

	public void setTlfNr(int tlfNr) {
		this.tlfNr = tlfNr;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAdministrator() {
		return administrator;
	}

	public void setAdministrator(boolean administrator) {
		this.administrator = administrator;
	}

	@Override
	public String toString() {
		return navn + " - (" + email + ")";
	}

}
