package application.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Konference extends Lokalitet {
	private String titel;
	private double pris;
	private int maxDeltagere;
	private LocalDateTime startDato;
	private LocalDateTime slutDato;
	private int tlfNr;
	private Tilstand tilstand;
	private ArrayList<Tilmelding> tilmeldeinger;
	private ArrayList<Udflugt> udflugter;
	private ArrayList<Hotel> hoteller;
	private ArrayList<Foredrag> foredrag;

	public Konference(String land, int postNr, String by, String vejNavn, String vejNr, String etage, String titel,
			double pris, int maxDeltagere, LocalDateTime startDato, LocalDateTime slutDato, int tlfNr) {
		super(land, postNr, by, vejNavn, vejNr, etage);
		this.titel = titel;
		this.pris = pris;
		this.maxDeltagere = maxDeltagere;
		this.startDato = startDato;
		this.slutDato = slutDato;
		this.tlfNr = tlfNr;
		this.tilmeldeinger = new ArrayList<>();
		this.udflugter = new ArrayList<>();
		this.hoteller = new ArrayList<>();
		this.foredrag = new ArrayList<>();
		this.tilstand = Tilstand.UDARBEJDES;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public double getPris() {
		return pris;
	}

	public void setPris(double pris) {
		this.pris = pris;
	}

	public int getMaxDeltagere() {
		return maxDeltagere;
	}

	public void setMaxDeltagere(int maxDeltagere) {
		this.maxDeltagere = maxDeltagere;
	}

	public LocalDateTime getStartDato() {
		return startDato;
	}

	public void setStartDato(LocalDateTime startDato) {
		this.startDato = startDato;
	}

	public LocalDateTime getSlutDato() {
		return slutDato;
	}

	public void setSlutDato(LocalDateTime slutDato) {
		this.slutDato = slutDato;
	}

	public int getTlfNr() {
		return tlfNr;
	}

	public void setTlfNr(int tlfNr) {
		this.tlfNr = tlfNr;
	}

	public ArrayList<Tilmelding> getTilmeldeinger() {
		return tilmeldeinger;
	}

	public ArrayList<Udflugt> getUdflugter() {
		return udflugter;
	}

	public ArrayList<Hotel> getHoteller() {
		return hoteller;
	}

	public ArrayList<Foredrag> getForedrag() {
		return foredrag;
	}

	public void addTilmelding(Tilmelding tilmelding) {
		this.tilmeldeinger.add(tilmelding);

		if (this.getTilmeldeinger().size() >= this.maxDeltagere) {
			this.setTilstand(Tilstand.LUKKET);
		}
	}

	public void deleteTilmelding(Tilmelding tilmelding) {
		this.tilmeldeinger.remove(tilmelding);

		if (this.getTilmeldeinger().size() < this.maxDeltagere) {
			this.setTilstand(Tilstand.ÅBEN);
		}
	}

	public void addUdflugt(Udflugt udflugt) {
		this.udflugter.add(udflugt);
	}

	public void deleteUdflugt(Udflugt udflugt) {
		this.udflugter.remove(udflugt);
	}

	public void addHotel(Hotel hotel) {
		this.hoteller.add(hotel);
	}

	public void deleteHotel(Hotel hotel) {
		this.hoteller.remove(hotel);
	}

	public void addForedrag(Foredrag foredrag) {
		this.foredrag.add(foredrag);
	}

	public void deleteForedrag(Foredrag foredrag) {
		this.foredrag.remove(foredrag);
	}

	public Tilstand getTilstand() {
		return tilstand;
	}

	public void setTilstand(Tilstand tilstand) {
		this.tilstand = tilstand;
	}

	@Override
	public String toString() {
		return titel + " - " + startDato.getDayOfMonth() + "/" + startDato.getMonthValue() + "/" + startDato.getYear()
				+ " - " + slutDato.getDayOfMonth() + "/" + slutDato.getMonthValue() + "/" + slutDato.getYear() + " : "
				+ tilstand;
	}

}
