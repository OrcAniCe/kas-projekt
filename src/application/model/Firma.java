package application.model;

public class Firma {
	private String navn;
	private String email;
	private int tlfNr;

	public Firma(String navn, String email, int tlfNr) {
		this.navn = navn;
		this.email = email;
		this.tlfNr = tlfNr;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getTlfNr() {
		return tlfNr;
	}

	public void setTlfNr(int tlfNr) {
		this.tlfNr = tlfNr;
	}

	@Override
	public String toString() {
		return navn + " (" + tlfNr + ")";
	}

}
