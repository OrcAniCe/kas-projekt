package application.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import application.model.Deltager;
import application.model.Firma;
import application.model.Foredrag;
import application.model.Hotel;
import application.model.HotelService;
import application.model.Konference;
import application.model.Tilmelding;
import application.model.Tilstand;
import application.model.Udflugt;
import storage.Storage;

public class Service {

	// Login
	// -------------------------------------------------------------------------
	public static Deltager deltagerLoggedIn;

	public static Deltager login(String email, String password) {
		Deltager deltager = null;

		ArrayList<Deltager> deltagere = new ArrayList<>();
		deltagere = Service.getDeltagere();

		for (int i = 0; i < deltagere.size(); i++) {
			if (deltagere.get(i).getEmail().equals(email) && deltagere.get(i).getPassword().equals(password)) {
				Service.deltagerLoggedIn = deltagere.get(i);
				return deltagere.get(i);
			}
		}
		return deltager;
	}

	public static void logud() {
		Service.deltagerLoggedIn = null;
	}

	// Deltager
	// -------------------------------------------------------------------------

	public static ArrayList<Deltager> getDeltagere() {
		return Storage.getDeltagere();
	}

	public static Deltager createDeltager(String land, int postNr, String by, String vejNavn, String vejNr,
			String etage, int tlfNr, String navn, String email, String password, boolean administrator) {
		Deltager deltager = new Deltager(land, postNr, by, vejNavn, vejNr, etage, tlfNr, navn, email, password,
				administrator);
		Storage.addDeltager(deltager);
		return deltager;
	}

	public static void deleteDeltager(Deltager deltager) {
		Storage.removeDeltager(deltager);
	}

	public static void updateDeltager(Deltager deltager, String land, int postNr, String by, String vejNavn,
			String vejNr, String etage, int tlfNr, String navn, String email, String password, boolean administrator) {
		deltager.setLand(land);
		deltager.setPostNr(postNr);
		deltager.setBy(by);
		deltager.setVejNavn(vejNavn);
		deltager.setVejNr(vejNr);
		deltager.setEtage(etage);
		deltager.setNavn(navn);
		deltager.setEmail(email);
		deltager.setPassword(password);
		deltager.setTlfNr(tlfNr);
		deltager.setAdministrator(administrator);
	}

	// Firma
	// -------------------------------------------------------------------------

	public static ArrayList<Firma> getFirmaer() {
		return Storage.getFirmaer();
	}

	public static Firma createFirma(String navn, String email, int tlfNr) {
		Firma firma = new Firma(navn, email, tlfNr);
		Storage.addFirma(firma);
		return firma;
	}

	public static void removeFirma(Firma firma) {
		Storage.removeFirma(firma);
	}

	// Foredrag
	// -------------------------------------------------------------------------

	public static Foredrag createForedrag(Konference konference, String titel, String beskrivelse,
			LocalDateTime startTid, LocalDateTime slutTid, ArrayList<Deltager> foredragsholder) {
		Foredrag foredrag = new Foredrag(titel, beskrivelse, startTid, slutTid, foredragsholder);
		konference.addForedrag(foredrag);
		return foredrag;
	}

	public static void updateForedrag(Foredrag foredrag, String titel, String beskrivelse, LocalDateTime startTid,
			LocalDateTime slutTid, ArrayList<Deltager> foredragsholder) {
		foredrag.setTitel(titel);
		foredrag.setBeskrivelse(beskrivelse);
		foredrag.setStartTid(startTid);
		foredrag.setSlutTid(slutTid);
		foredrag.setForedragsholder(foredragsholder);
	}

	public static void removeForedrag(Konference konference, Foredrag foredrag) {
		konference.deleteForedrag(foredrag);
	}

	// Tilmelding
	// -------------------------------------------------------------------------
	public static Tilmelding createTilmelding(LocalDate ankomstDato, LocalDate afrejseDato, String ledsagerNavn,
			Firma firma, Deltager deltager, Hotel hotel, Konference konference, ArrayList<Udflugt> udflugter,
			ArrayList<HotelService> hotelServices) {
		Tilmelding tilmelding = new Tilmelding(ankomstDato, afrejseDato, ledsagerNavn, firma, deltager, hotel,
				konference, udflugter, hotelServices);
		konference.addTilmelding(tilmelding);
		return tilmelding;
	}

	public static void updateTilmelding(Tilmelding tilmelding, LocalDate ankomstDato, LocalDate afrejseDato,
			String ledsagerNavn, Firma firma, Hotel hotel, ArrayList<Udflugt> udflugter,
			ArrayList<HotelService> hotelServices) {
		tilmelding.setAnkomstDato(ankomstDato);
		tilmelding.setAfrejseDato(afrejseDato);
		tilmelding.setLedsagerNavn(ledsagerNavn);
		tilmelding.setFirma(firma);
		tilmelding.setHotel(hotel);
		tilmelding.setUdflugter(udflugter);
		tilmelding.setHotelServices(hotelServices);
	}

	public static void deleteTilmelding(Tilmelding tilmelding, Konference konference) {
		konference.deleteTilmelding(tilmelding);
	}

	// Konference
	// -------------------------------------------------------------------------
	public static ArrayList<Konference> getKonferencer() {
		return Storage.getKonferencer();
	}

	public static Konference createKonference(String land, int postNr, String by, String vejNavn, String vejNr,
			String etage, String titel, double pris, int maxDeltagere, LocalDateTime startDato, LocalDateTime slutDato,
			int tlfNr) {
		Konference konference = new Konference(land, postNr, by, vejNavn, vejNr, etage, titel, pris, maxDeltagere,
				startDato, slutDato, tlfNr);
		Storage.addKonference(konference);
		return konference;
	}

	public static void updateKonference(Konference konference, String land, int postNr, String by, String vejNavn,
			String vejNr, String etage, String titel, double pris, int maxDeltagere, LocalDateTime startDato,
			LocalDateTime slutDato, int tlfNr, Tilstand tilstand) {
		konference.setLand(land);
		konference.setPostNr(postNr);
		konference.setBy(by);
		konference.setVejNavn(vejNavn);
		konference.setVejNr(vejNr);
		konference.setEtage(etage);
		konference.setTitel(titel);
		konference.setPris(pris);
		konference.setMaxDeltagere(maxDeltagere);
		konference.setStartDato(startDato);
		konference.setSlutDato(slutDato);
		konference.setTlfNr(tlfNr);
		konference.setTilstand(tilstand);
	}

	public static void removeKonference(Konference konference) {
		Storage.removeKonference(konference);
	}

	// Hotel
	// -------------------------------------------------------------------------
	public static Hotel createHotel(Konference konference, String navn, int tlfNr, String adresse, double prisEnkelt,
			double prisDobbelt, ArrayList<HotelService> services) {
		Hotel hotel = new Hotel(navn, tlfNr, adresse, prisEnkelt, prisDobbelt, services);
		konference.addHotel(hotel);
		return hotel;
	}

	public static void updateHotel(Hotel hotel, String navn, int tlfNr, String adresse, double prisEnkelt,
			double prisDobbelt, ArrayList<HotelService> services) {
		hotel.setNavn(navn);
		hotel.setTlfNr(tlfNr);
		hotel.setAdresse(adresse);
		hotel.setPrisEnkelt(prisEnkelt);
		hotel.setPrisDobbelt(prisDobbelt);
		hotel.setServices(services);
	}

	public static void removeHotel(Konference konference, Hotel hotel) {
		konference.deleteHotel(hotel);
	}

	// HotelService
	// -------------------------------------------------------------------------
	public static HotelService createHotelService(String navn, double pris) {
		HotelService hotelService = new HotelService(navn, pris);
		return hotelService;
	}

	// Udflugt
	// -------------------------------------------------------------------------
	public static Udflugt createUdflugt(Konference konference, String navn, String beskrivelse, double pris,
			LocalDateTime startTid, LocalDateTime slutTid) {
		Udflugt udflugt = new Udflugt(navn, beskrivelse, pris, startTid, slutTid);
		konference.addUdflugt(udflugt);
		return udflugt;
	}

	public static void updateUdflugt(Udflugt udflugt, String navn, String beskrivelse, double pris,
			LocalDateTime startTid, LocalDateTime slutTid) {
		udflugt.setNavn(navn);
		udflugt.setBeskrivelse(beskrivelse);
		udflugt.setPris(pris);
		udflugt.setStartTid(startTid);
		udflugt.setSlutTid(slutTid);
	}

	public static void removeUdflugt(Konference konference, Udflugt udflugt) {
		konference.deleteUdflugt(udflugt);
	}

	/**
	 * Initializes the storage with some objects.
	 */
	public static void initStorage() {
		Konference k6 = Service.createKonference("England", 1010, "London", "Leicester square", "24", "ST. TV",
				"Kunsten Vidunderlige Verden", 8999, 2100, LocalDateTime.of(2014, 3, 11, 12, 00),
				LocalDateTime.of(2014, 3, 13, 14, 00), 77988888);
		Konference k5 = Service.createKonference("Danmark", 7730, "Hanstholm", "Kai Lindbergsgade", "77", "ST. TH",
				"Jagt og Fiskeri", 4499, 270, LocalDateTime.of(2015, 2, 10, 10, 00),
				LocalDateTime.of(2015, 2, 12, 15, 00), 77888888);
		Konference k1 = Service.createKonference("Danmark", 2500, "Valby", "P. Knudsens Gade", "54", "4. TV",
				"Milj� og Teknik", 2990, 350, LocalDateTime.of(2016, 5, 15, 15, 00),
				LocalDateTime.of(2016, 5, 17, 15, 00), 78888888);
		Konference k2 = Service.createKonference("Danmark", 8000, "�rhus", "Vestergade", "33", "ST. TV",
				"Alt godt fra havet", 1500, 500, LocalDateTime.of(2016, 6, 15, 15, 00),
				LocalDateTime.of(2016, 6, 17, 15, 00), 22334455);
		Konference k3 = Service.createKonference("Tyskland", 9999, "Berlin", "Brandenburger tor", "22", "2. TV",
				"Wienerschnitzel", 3999, 1200, LocalDateTime.of(2016, 12, 16, 19, 00),
				LocalDateTime.of(2016, 12, 20, 12, 00), 97932257);
		Konference k4 = Service.createKonference("Danmark", 9100, "�lborg", "Jomfru Ane Gade", "63", "ST. TH",
				"Bartending at its finest", 4475, 80, LocalDateTime.of(2016, 8, 2, 17, 00),
				LocalDateTime.of(2016, 8, 4, 16, 00), 45759515);
		// --------------------------
		Service.updateKonference(k1, "Danmark", 2500, "Valby", "P. Knudsens Gade", "54", "4. TV", "Milj� og Teknik",
				2990, 350, LocalDateTime.of(2016, 5, 15, 15, 00), LocalDateTime.of(2016, 5, 17, 15, 00), 78888888,
				Tilstand.ÅBEN);
		Service.updateKonference(k2, "Danmark", 8000, "�rhus", "Vestergade", "33", "ST. TV", "Alt godt fra havet", 1500,
				500, LocalDateTime.of(2016, 6, 15, 15, 00), LocalDateTime.of(2016, 6, 17, 15, 00), 22334455,
				Tilstand.ÅBEN);
		Service.updateKonference(k3, "Tyskland", 9999, "Berlin", "Brandenburger tor", "22", "2. TV", "Wienerschnitzel",
				3999, 1200, LocalDateTime.of(2016, 12, 16, 19, 00), LocalDateTime.of(2016, 12, 20, 12, 00), 97932257,
				Tilstand.ÅBEN);
		Service.updateKonference(k4, "Danmark", 9100, "�lborg", "Jomfru Ane gade", "63", "ST. TH",
				"Bartending at its finest", 4475, 80, LocalDateTime.of(2016, 8, 2, 17, 00),
				LocalDateTime.of(2016, 8, 4, 16, 00), 45759515, Tilstand.UDARBEJDES);
		Service.updateKonference(k5, "Danmark", 7730, "Hanstholm", "Kai Lindbergsgade", "77", "ST. TH",
				"Jagt og Fiskeri", 4499, 270, LocalDateTime.of(2015, 2, 10, 8, 00),
				LocalDateTime.of(2015, 2, 12, 15, 00), 77888888, Tilstand.LUKKET);
		Service.updateKonference(k6, "England", 1010, "London", "Leicester square", "24", "ST. TV",
				"Kunsten Vidunderlige Verden", 8999, 2100, LocalDateTime.of(2014, 3, 11, 12, 00),
				LocalDateTime.of(2014, 3, 13, 14, 00), 77988888, Tilstand.LUKKET);

		// Hotel Services
		HotelService hs1 = Service.createHotelService("Wifi", 25);
		HotelService hs2 = Service.createHotelService("Wifi", 89);
		HotelService hs3 = Service.createHotelService("Wifi", 120);
		HotelService hs4 = Service.createHotelService("Wifi", 60);
		HotelService hs5 = Service.createHotelService("Wifi", 50);
		HotelService hs6 = Service.createHotelService("Wifi", 80);
		HotelService hs7 = Service.createHotelService("Wifi", 90);
		HotelService hs8 = Service.createHotelService("Wifi", 35);
		HotelService hs9 = Service.createHotelService("Wifi", 45);
		HotelService hs10 = Service.createHotelService("Wifi", 100);
		HotelService hs11 = Service.createHotelService("Wifi", 95);
		HotelService hs12 = Service.createHotelService("Wifi", 50);
		HotelService hs13 = Service.createHotelService("Wifi", 75);
		HotelService hs14 = Service.createHotelService("Wifi", 49);
		HotelService hs15 = Service.createHotelService("Wifi", 30);
		HotelService hs16 = Service.createHotelService("Wifi", 50);
		HotelService hs17 = Service.createHotelService("Wifi", 75);
		HotelService hs18 = Service.createHotelService("Wifi", 150);
		HotelService hs19 = Service.createHotelService("Eget bad", 129);
		HotelService hs20 = Service.createHotelService("Mini Bar", 330);
		HotelService hs21 = Service.createHotelService("Eget bad", 250);
		HotelService hs22 = Service.createHotelService("Eget bad", 175);
		HotelService hs23 = Service.createHotelService("Mini Bar", 299);
		HotelService hs24 = Service.createHotelService("Eget bad", 159);
		HotelService hs25 = Service.createHotelService("Eget bad", 150);
		HotelService hs26 = Service.createHotelService("Mini Bar", 350);
		HotelService hs27 = Service.createHotelService("Eget bad", 190);
		HotelService hs28 = Service.createHotelService("Eget bad", 185);
		HotelService hs29 = Service.createHotelService("Eget bad", 235);
		HotelService hs30 = Service.createHotelService("Mini Bar", 499);
		HotelService hs31 = Service.createHotelService("Mini Bar", 400);
		HotelService hs32 = Service.createHotelService("Eget bad", 200);
		HotelService hs33 = Service.createHotelService("Eget bad", 140);
		HotelService hs34 = Service.createHotelService("Eget bad", 240);
		HotelService hs35 = Service.createHotelService("Mini Bar", 300);
		HotelService hs36 = Service.createHotelService("Eget bad", 190);
		HotelService hs37 = Service.createHotelService("Kabel TV", 129);
		HotelService hs38 = Service.createHotelService("Boblebad", 335);
		HotelService hs39 = Service.createHotelService("Sauna", 275);
		HotelService hs40 = Service.createHotelService("Butler", 675);
		HotelService hs41 = Service.createHotelService("Vink�lder", 299);
		HotelService hs42 = Service.createHotelService("Kabel TV", 159);
		HotelService hs43 = Service.createHotelService("Surround Sound Speakers", 150);
		HotelService hs44 = Service.createHotelService("Sauna", 350);
		HotelService hs45 = Service.createHotelService("24 Timer Concierges Service", 590);
		HotelService hs46 = Service.createHotelService("Vink�lder", 285);
		HotelService hs47 = Service.createHotelService("Tjener", 350);
		HotelService hs48 = Service.createHotelService("Limosine Service", 499);
		HotelService hs49 = Service.createHotelService("Butler", 400);
		HotelService hs50 = Service.createHotelService("24 Timers Massage", 350);
		HotelService hs51 = Service.createHotelService("24 Timers Gym", 99);
		HotelService hs52 = Service.createHotelService("Kabel TV", 145);
		HotelService hs53 = Service.createHotelService("Limosine Service", 400);
		HotelService hs54 = Service.createHotelService("Boblebad", 195);

		// Service tilknyttet hotel

		ArrayList<HotelService> hos1 = new ArrayList<>();
		hos1.add(hs1);
		hos1.add(hs19);
		hos1.add(hs37);

		ArrayList<HotelService> hos2 = new ArrayList<>();
		hos2.add(hs2);
		hos2.add(hs20);
		hos2.add(hs38);

		ArrayList<HotelService> hos3 = new ArrayList<>();
		hos3.add(hs3);
		hos3.add(hs21);
		hos3.add(hs39);

		ArrayList<HotelService> hos4 = new ArrayList<>();
		hos4.add(hs4);
		hos4.add(hs22);
		hos4.add(hs40);

		ArrayList<HotelService> hos5 = new ArrayList<>();
		hos5.add(hs5);
		hos5.add(hs23);
		hos5.add(hs41);

		ArrayList<HotelService> hos6 = new ArrayList<>();
		hos6.add(hs6);
		hos6.add(hs24);
		hos6.add(hs42);

		ArrayList<HotelService> hos7 = new ArrayList<>();
		hos7.add(hs7);
		hos7.add(hs25);
		hos7.add(hs43);

		ArrayList<HotelService> hos8 = new ArrayList<>();
		hos8.add(hs8);
		hos8.add(hs26);
		hos8.add(hs44);

		ArrayList<HotelService> hos9 = new ArrayList<>();
		hos9.add(hs9);
		hos9.add(hs27);
		hos9.add(hs45);

		ArrayList<HotelService> hos10 = new ArrayList<>();
		hos10.add(hs10);
		hos10.add(hs28);
		hos10.add(hs46);

		ArrayList<HotelService> hos11 = new ArrayList<>();
		hos11.add(hs11);
		hos11.add(hs29);
		hos11.add(hs47);

		ArrayList<HotelService> hos12 = new ArrayList<>();
		hos12.add(hs12);
		hos12.add(hs30);
		hos12.add(hs48);

		ArrayList<HotelService> hos13 = new ArrayList<>();
		hos13.add(hs13);
		hos13.add(hs31);
		hos13.add(hs49);

		ArrayList<HotelService> hos14 = new ArrayList<>();
		hos14.add(hs14);
		hos14.add(hs32);
		hos14.add(hs50);

		ArrayList<HotelService> hos15 = new ArrayList<>();
		hos15.add(hs15);
		hos15.add(hs33);
		hos15.add(hs51);

		ArrayList<HotelService> hos16 = new ArrayList<>();
		hos16.add(hs16);
		hos16.add(hs34);
		hos16.add(hs52);

		ArrayList<HotelService> hos17 = new ArrayList<>();
		hos17.add(hs17);
		hos17.add(hs35);
		hos17.add(hs53);

		ArrayList<HotelService> hos18 = new ArrayList<>();
		hos18.add(hs18);
		hos18.add(hs36);
		hos18.add(hs54);

		// Hoteller
		Hotel h1 = Service.createHotel(k1, "Hilton", 55555532, "Strandvejen 13", 1200, 1900, hos1);
		Hotel h2 = Service.createHotel(k1, "Ritz", 55887523, "Hellerupvej 10", 900, 1400, hos2);
		Hotel h3 = Service.createHotel(k1, "D'angleterre", 33120095, "Kongensnytorv 34", 3500, 6000, hos3);
		Hotel h4 = Service.createHotel(k2, "Bjarnes Kro", 49087511, "Park Alle", 450, 550, hos4);
		Hotel h5 = Service.createHotel(k2, "Hotel Sandholm", 34536675, "Parkvej 12", 299, 499, hos5);
		Hotel h6 = Service.createHotel(k2, "Cabinn", 22887544, "Skolegade 30", 599, 649, hos6);
		Hotel h7 = Service.createHotel(k3, "Bamse Bolchebutik", 11223366, "Tingstrupvej 167", 500, 800, hos7);
		Hotel h8 = Service.createHotel(k3, "Strandhotellet", 23138875, "Tilstedvej 22", 350, 470, hos8);
		Hotel h9 = Service.createHotel(k3, "Hotel Oasis", 66887511, "Istedgade 24", 300, 450, hos9);
		Hotel h10 = Service.createHotel(k4, "Birgers Vandrehjem", 95588744, "Katholmen 87", 799, 999, hos10);
		Hotel h11 = Service.createHotel(k4, "�lborg Motel", 53598875, "H�jen 34", 199, 275, hos11);
		Hotel h12 = Service.createHotel(k4, "Radisson", 55887522, "Helenelyst 9", 375, 475, hos12);
		Hotel h13 = Service.createHotel(k5, "Scandic", 55887511, "N�rrebrogade 22", 900, 1100, hos13);
		Hotel h14 = Service.createHotel(k5, "Park Inn", 11558875, "N�rregade 17", 775, 975, hos14);
		Hotel h15 = Service.createHotel(k5, "Copenhagen Plaza", 21558875, "H.C Andersensvej 21", 875, 999, hos15);
		Hotel h16 = Service.createHotel(k6, "Marriot Hotel", 42668875, "Busgaden 19", 599, 799, hos16);
		Hotel h17 = Service.createHotel(k6, "Hotel Maritime", 19558875, "Viby Torv 88", 300, 400, hos17);
		Hotel h18 = Service.createHotel(k6, "Hotel Tiffany", 37549975, "Skyumvej 65", 500, 600, hos18);

		// Udflugter

		Udflugt u2 = Service.createUdflugt(k1, "Vinter badning", "S� lille", 149.95,
				LocalDateTime.of(2016, 5, 16, 10, 00), LocalDateTime.of(2016, 5, 16, 14, 00));
		Udflugt u3 = Service.createUdflugt(k1, "Aros", "Ud og se", 179, LocalDateTime.of(2016, 5, 17, 10, 00),
				LocalDateTime.of(2016, 5, 17, 15, 00));
		Udflugt u1 = Service.createUdflugt(k1, "Shopping", "Bruuns Galleri", 50, LocalDateTime.of(2016, 5, 15, 16, 00),
				LocalDateTime.of(2016, 5, 15, 20, 00));
		// ----------------

		Udflugt u4 = Service.createUdflugt(k2, "SightSeeing", "Rundt i Byen", 149,
				LocalDateTime.of(2016, 6, 16, 10, 00), LocalDateTime.of(2016, 6, 16, 15, 00));
		Udflugt u5 = Service.createUdflugt(k2, "Spa og Wellness", "Fork�lelse af kroppen", 279,
				LocalDateTime.of(2016, 5, 15, 16, 00), LocalDateTime.of(2016, 6, 15, 21, 00));
		Udflugt u6 = Service.createUdflugt(k2, "Rold Skov", "Ud og se", 229, LocalDateTime.of(2016, 6, 17, 10, 00),
				LocalDateTime.of(2016, 6, 17, 14, 30));
		// ----------------

		Udflugt u7 = Service.createUdflugt(k3, "Den Gamle By", "M�rk Jule Stemningen", 235,
				LocalDateTime.of(2016, 12, 17, 12, 00), LocalDateTime.of(2016, 12, 20, 17, 30));
		Udflugt u8 = Service.createUdflugt(k3, "Golf", "Ud og spille", 180, LocalDateTime.of(2016, 12, 18, 10, 00),
				LocalDateTime.of(2016, 12, 18, 19, 00));
		Udflugt u9 = Service.createUdflugt(k3, "Frigatten Jylland", "Ebeltoft", 149,
				LocalDateTime.of(2016, 12, 19, 9, 30), LocalDateTime.of(2016, 12, 19, 18, 00));
		// ----------------

		Udflugt u10 = Service.createUdflugt(k4, "Moesgaard Museum", "Ud og se", 139,
				LocalDateTime.of(2016, 8, 3, 9, 00), LocalDateTime.of(2016, 8, 3, 14, 30));
		Udflugt u11 = Service.createUdflugt(k4, "Udflugt til stranden", "Ud og se", 89,
				LocalDateTime.of(2016, 8, 3, 14, 45), LocalDateTime.of(2016, 8, 3, 20, 00));
		Udflugt u12 = Service.createUdflugt(k4, "Wellness", "Forplejning", 279, LocalDateTime.of(2016, 8, 4, 11, 00),
				LocalDateTime.of(2016, 8, 4, 15, 00));

		// -----------------

		Udflugt u13 = Service.createUdflugt(k5, "Hanstholm Bunker", "Ud og se", 129,
				LocalDateTime.of(2015, 2, 10, 10, 30), LocalDateTime.of(2015, 2, 10, 16, 00));
		Udflugt u14 = Service.createUdflugt(k5, "Restuarant den Gyldne Reje", "Ud og spise", 350,
				LocalDateTime.of(2015, 2, 10, 16, 30), LocalDateTime.of(2015, 2, 10, 21, 30));
		Udflugt u15 = Service.createUdflugt(k5, "Vigs� Legeland", "Ud og se", 259,
				LocalDateTime.of(2015, 2, 11, 10, 30), LocalDateTime.of(2015, 2, 11, 17, 30));

		Udflugt u16 = Service.createUdflugt(k6, "Kanal Rundfart", "Ud og Opleve K�benhavn", 279,
				LocalDateTime.of(2014, 3, 11, 13, 00), LocalDateTime.of(2014, 3, 11, 18, 45));
		Udflugt u17 = Service.createUdflugt(k6, "BolcheBageriet", "Lav dit eget Bolche", 379,
				LocalDateTime.of(2014, 3, 12, 8, 30), LocalDateTime.of(2014, 3, 12, 17, 00));
		Udflugt u18 = Service.createUdflugt(k6, "Tycho Brahe Planetarium", "Ud og se", 259,
				LocalDateTime.of(2014, 3, 13, 8, 45), LocalDateTime.of(2014, 3, 13, 13, 30));

		// Deltager
		Deltager d1 = Service.createDeltager("Danmark", 8471, "Sabro", "Sabro Skolevej", "13", "", 30633058,
				"Thomas Madsen", "zynzz@zynzz.dk", "1234", true);
		Deltager d2 = Service.createDeltager("Danmark", 8000, "�rhus", "Vejen", "14A", "2. TV", 88888888, "Jens",
				"jens@jens.dk", "1234", false);
		Deltager d3 = Service.createDeltager("Danmark", 8000, "�rhus", "Vestergade", "5b", "2. TV", 81618369,
				"Jonas Thomsen", "jonasgt1@hotmail.com", "1234", true);
		Deltager d4 = Service.createDeltager("Danmark", 8250, "Eg�", "Bredk�r Parkvej", "64", "", 23456789,
				"Andreas Elkj�r", "elprebz@gmail.com", "1234", true);

		// Firma
		Firma fm1 = Service.createFirma("Maersk A/S", "maersk@maersk.dk", 23232818);
		Firma fm2 = Service.createFirma("L'easy", "leasy@leasy.dk", 88888888);
		Firma fm3 = Service.createFirma("Computer Gruppen ApS", "com@cg.dk", 23232818);
		Firma fm4 = Service.createFirma("Jem og Fix", "jemogfix@jemogfix", 23277718);
		Firma fm5 = Service.createFirma("Silvan", "silvan@silvan.dk", 23277758);

		// Tilmelding
		ArrayList<Udflugt> udflugter1 = new ArrayList<>();
		udflugter1.add(u1);
		udflugter1.add(u2);
		udflugter1.add(u3);
		ArrayList<HotelService> hService1 = new ArrayList<>();
		hService1.add(hs1);
		hService1.add(hs19);
		hService1.add(hs37);
		Tilmelding t1 = Service.createTilmelding(LocalDate.of(2016, 5, 15), LocalDate.of(2016, 5, 17), "Birthe Larsen",
				fm1, d1, h1, k1, udflugter1, hService1);

		ArrayList<Udflugt> udflugter2 = new ArrayList<>();
		udflugter2.add(u4);
		udflugter2.add(u5);
		ArrayList<HotelService> hService2 = new ArrayList<>();
		hService2.add(hs2);
		hService2.add(hs20);
		hService2.add(hs38);
		Tilmelding t2 = Service.createTilmelding(LocalDate.of(2016, 6, 15), LocalDate.of(2016, 6, 17), "Janni Kj�r",
				fm3, d1, h2, k2, udflugter2, hService2);

		ArrayList<Udflugt> udflugter3 = new ArrayList<>();
		udflugter3.add(u2);
		udflugter3.add(u1);
		udflugter3.add(u3);
		ArrayList<HotelService> hService3 = new ArrayList<>();
		hService3.add(hs3);
		hService3.add(hs21);
		hService3.add(hs39);
		Tilmelding t3 = Service.createTilmelding(LocalDate.of(2016, 5, 15), LocalDate.of(2016, 5, 17), "Fr�ken Olsen",
				null, d3, h3, k1, udflugter3, hService3);

		// Foredragsholder Array

		ArrayList<Deltager> fh1 = new ArrayList<>();
		fh1.add(d1);
		ArrayList<Deltager> fh2 = new ArrayList<>();
		fh2.add(d1);
		ArrayList<Deltager> fh3 = new ArrayList<>();
		fh3.add(d2);
		ArrayList<Deltager> fh4 = new ArrayList<>();
		fh4.add(d1);
		ArrayList<Deltager> fh5 = new ArrayList<>();
		fh5.add(d1);
		ArrayList<Deltager> fh6 = new ArrayList<>();
		fh6.add(d1);
		ArrayList<Deltager> fh7 = new ArrayList<>();
		fh7.add(d2);
		ArrayList<Deltager> fh8 = new ArrayList<>();
		fh8.add(d1);
		ArrayList<Deltager> fh9 = new ArrayList<>();
		fh9.add(d1);
		ArrayList<Deltager> fh10 = new ArrayList<>();
		fh10.add(d1);
		ArrayList<Deltager> fh11 = new ArrayList<>();
		fh11.add(d1);
		ArrayList<Deltager> fh12 = new ArrayList<>();
		fh12.add(d2);
		ArrayList<Deltager> fh13 = new ArrayList<>();
		fh13.add(d1);
		ArrayList<Deltager> fh14 = new ArrayList<>();
		fh14.add(d2);
		ArrayList<Deltager> fh15 = new ArrayList<>();
		fh15.add(d1);
		ArrayList<Deltager> fh16 = new ArrayList<>();
		fh16.add(d1);
		ArrayList<Deltager> fh17 = new ArrayList<>();
		fh17.add(d1);
		ArrayList<Deltager> fh18 = new ArrayList<>();
		fh18.add(d1);
		ArrayList<Deltager> fh19 = new ArrayList<>();
		fh19.add(d2);

		// Foredrag

		Foredrag f1 = Service.createForedrag(k1, "Nedk�mpe CO2", "Gr�nnere Milj�",
				LocalDateTime.of(2016, 5, 15, 16, 00), LocalDateTime.of(2016, 5, 15, 22, 45), fh1);
		Foredrag f2 = Service.createForedrag(k1, "Fiskernes Overlevelse", "Opl�g om fiskebestanden",
				LocalDateTime.of(2016, 5, 16, 8, 45), LocalDateTime.of(2016, 5, 16, 17, 45), fh2);
		Foredrag f3 = Service.createForedrag(k1, "Fiskeri Workshop", "Parter en fisk",
				LocalDateTime.of(2016, 5, 17, 9, 00), LocalDateTime.of(2016, 5, 17, 14, 45), fh3);

		Foredrag f4 = Service.createForedrag(k2, "Fiskeri Workshop", "Parter en fisk",
				LocalDateTime.of(2016, 6, 15, 15, 30), LocalDateTime.of(2016, 6, 15, 22, 45), fh4);
		Foredrag f5 = Service.createForedrag(k2, "Jagt Workshop", "Slagt et R�dyr",
				LocalDateTime.of(2016, 6, 16, 9, 00), LocalDateTime.of(2016, 6, 16, 16, 45), fh5);
		Foredrag f6 = Service.createForedrag(k2, "Jagttegn", "L�r at skyde med riffel",
				LocalDateTime.of(2016, 6, 17, 9, 00), LocalDateTime.of(2016, 6, 17, 14, 45), fh6);

		Foredrag f7 = Service.createForedrag(k3, "Tysklands �konomi", "Makro �konomi",
				LocalDateTime.of(2016, 12, 16, 19, 30), LocalDateTime.of(2016, 12, 16, 23, 40), fh7);
		Foredrag f8 = Service.createForedrag(k3, "Det Tyske K�kken", "Chef Franz",
				LocalDateTime.of(2016, 12, 17, 10, 00), LocalDateTime.of(2016, 12, 17, 18, 00), fh8);
		Foredrag f9 = Service.createForedrag(k3, "Sport i Tyskland", "Sportsministeren",
				LocalDateTime.of(2016, 12, 18, 10, 00), LocalDateTime.of(2016, 6, 18, 17, 30), fh9);

		Foredrag f10 = Service.createForedrag(k4, "Nye Drinks", "Nyeste Trend", LocalDateTime.of(2016, 8, 2, 17, 30),
				LocalDateTime.of(2016, 8, 2, 23, 30), fh10);
		Foredrag f11 = Service.createForedrag(k4, "Flair", "Danmarksmester", LocalDateTime.of(2016, 8, 3, 10, 00),
				LocalDateTime.of(2016, 8, 3, 18, 00), fh11);
		Foredrag f12 = Service.createForedrag(k4, "Indretningen", "Indretningstips",
				LocalDateTime.of(2016, 8, 3, 18, 30), LocalDateTime.of(2016, 8, 3, 22, 30), fh12);
		Foredrag f13 = Service.createForedrag(k4, "DJ", "Musikkens Verden", LocalDateTime.of(2016, 8, 4, 10, 00),
				LocalDateTime.of(2016, 8, 4, 15, 30), fh13);

		Foredrag f14 = Service.createForedrag(k5, "Fisk", "Fisk i alle farver", LocalDateTime.of(2015, 2, 10, 10, 30),
				LocalDateTime.of(2015, 2, 10, 23, 00), fh14);
		Foredrag f15 = Service.createForedrag(k5, "Jagt", "Fugle", LocalDateTime.of(2015, 2, 11, 10, 45),
				LocalDateTime.of(2015, 2, 11, 14, 40), fh15);
		Foredrag f16 = Service.createForedrag(k5, "Jagt", "R�dyr", LocalDateTime.of(2015, 2, 12, 9, 30),
				LocalDateTime.of(2015, 2, 12, 14, 30), fh16);

		Foredrag f17 = Service.createForedrag(k6, "Madame Tussauds", "Voks", LocalDateTime.of(2014, 3, 11, 12, 30),
				LocalDateTime.of(2014, 3, 11, 19, 30), fh17);
		Foredrag f18 = Service.createForedrag(k6, "Barokkens Verden", "Om Barokken",
				LocalDateTime.of(2014, 3, 12, 9, 30), LocalDateTime.of(2014, 3, 12, 16, 30), fh18);
		Foredrag f19 = Service.createForedrag(k6, "Modernisme", "Om Modernismen", LocalDateTime.of(2014, 3, 13, 8, 30),
				LocalDateTime.of(2014, 3, 13, 13, 30), fh19);

	}
}