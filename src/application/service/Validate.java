package application.service;

public class Validate {
	public static boolean isValidEmail(String email) {
		String regx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(regx);
		java.util.regex.Matcher m = p.matcher(email);
		return m.matches();
	}

	public static boolean isValidInteger(String string) {
		try {
			Integer.parseInt(string);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isValidName(String name) {
		String regx = "^[\\p{L} .'-]+$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(regx);
		java.util.regex.Matcher m = p.matcher(name);
		return m.matches();
	}

}
