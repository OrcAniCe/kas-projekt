package storage;

import java.util.ArrayList;

import application.model.Deltager;
import application.model.Firma;
import application.model.Konference;

public class Storage {
	private static ArrayList<Deltager> deltagere = new ArrayList<>();
	private static ArrayList<Firma> firmaer = new ArrayList<>();
	private static ArrayList<Konference> konferencer = new ArrayList<>();

	// Deltager
	// -------------------------------------------------------------------------

	public static ArrayList<Deltager> getDeltagere() {
		return new ArrayList<Deltager>(deltagere);
	}

	public static void addDeltager(Deltager deltager) {
		deltagere.add(deltager);
	}

	public static void removeDeltager(Deltager deltager) {
		deltagere.remove(deltager);
	}

	// Firma
	// -------------------------------------------------------------------------

	public static ArrayList<Firma> getFirmaer() {
		return new ArrayList<Firma>(firmaer);
	}

	public static void addFirma(Firma firma) {
		firmaer.add(firma);
	}

	public static void removeFirma(Firma firma) {
		firmaer.remove(firma);
	}

	// Konference
	// -------------------------------------------------------------------------

	public static ArrayList<Konference> getKonferencer() {
		return new ArrayList<Konference>(konferencer);
	}

	public static void addKonference(Konference konference) {
		konferencer.add(konference);
	}

	public static void removeKonference(Konference konference) {
		konferencer.remove(konference);
	}
}
